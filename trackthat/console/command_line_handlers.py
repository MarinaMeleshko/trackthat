"""This module provides command line handlers"""


import datetime

from trackthat.config import DATE_FORMAT, TIME_FORMAT
from trackthat.library.model.plan import Period


def change_user(args, controller):
    username = args.username
    user = controller.find_user(username)
    if user is None:
        raise ValueError("User with username %s doesn't exist" % username)
    controller.log_in(user)
    print("Current user is ", user.name)


def get_current_user(args, controller):
    cur_user = controller.get_current_user()
    if cur_user is None:
        print("You're not authorized")
    else:
        print(cur_user)


def get_all_users(args, controller):
    for u in controller.get_all_users():
        print(u.name)


def add_user(args, controller):
    username = args.username
    duplicate = controller.find_user(username)
    if duplicate is not None:
        raise ValueError("User with username %s exist" % username)
    controller.add_user(username)
    print("Added new user %s" % username)


def parse_date(args):
    """Parse date from string or from components day, month, year"""

    if args.date is not None:
        date = datetime.datetime.strptime(args.date, DATE_FORMAT)
        date = date.date()
    else:
        today = datetime.date.today()
        if args.day is not None:
            day = int(args.day)
        else:
            day = today.day
        if args.month is not None:
            month = int(args.moth)
        else:
            month = today.month
        if args.year is not None:
            year = int(args.year)
        else:
            year = today.year
        date = datetime.date(day=day, month=month, year=year)
    return date


def get_date(args, controller):
    """Get and print date from user calendar with all activities"""

    if args.date is not None and (args.day is not None or args.month is not None or args.year is not None):
        raise ValueError("Argument date isn't allowed with arguments day, month and year")

    date = parse_date(args)
    try:
        date_instance = controller.get_date(date)
    except ValueError:
        print("There isn't any activities to this date")
    else:
        print(date_instance)


def get_lists(args, controller):
    lists = []
    if args.action is True:
        lists += controller.get_action_lists()
    if args.task is True:
        lists += controller.get_task_lists()

    if args.action is False and args.task is False:
        lists = controller.get_action_lists() + controller.get_task_lists()
    for l in lists:
            print(l.name)


def show_list(args, controller):
    name = args.listname
    user_list = controller.get_list(name)
    if user_list is None:
        print("You haven't list with name %s" % name)
    else:
        print(user_list)


def create_list(args, controller):
    if args.action is True and args.task is True:
        raise ValueError("Argument action isn't allowed with argument task")

    list_name = args.name
    if args.action is True:
        controller.create_action_list(list_name)
    elif args.task is True:
        controller.create_task_list(list_name)
    else:
        print("Action or task list?")


def remove_list(args, controller):
    list_name = args.name
    controller.remove_list(list_name)


def add_user_to_list(args, controller):
    user_name = args.user
    list_name = args.list
    controller.add_user_to_list(user_name, list_name)


def remove_user_from_list(args, controller):
    user_name = args.user
    list_name = args.list
    controller.remove_user_from_list(user_name, list_name)


def rename_list(args, controller):
    old_name = args.oldname
    new_name = args.newname
    controller.rename_list(old_name, new_name)


def create_action(args, controller):
    name = args.name
    comment = args.comment
    if args.deadline is not None:
        deadline = datetime.datetime.strptime(args.deadline, DATE_FORMAT).date()
    else:
        deadline = None
    list_name = args.list
    controller.create_action(name, deadline, comment, list_name)


def edit_action(args, controller):
    action_id = int(args.id)
    name = args.name
    comment = args.comment
    if args.deadline is not None:
        deadline = datetime.datetime.strptime(args.deadline, DATE_FORMAT).date()
    else:
        deadline = None
    controller.edit_action(action_id, name, deadline, comment)


def remove_action(args, controller):
    action_id = args.id
    controller.remove_action(action_id)


def create_event(args, controller):
    if args.private and args.public:
        raise ValueError("Arguments private and public aren't allowed together")

    name = args.name
    if args.date is not None:
        date = datetime.datetime.strptime(args.date, DATE_FORMAT).date()
    else:
        date = None
    if args.begin is not None:
        begin = datetime.datetime.strptime(args.begin, TIME_FORMAT).time()
    else:
        begin = None
    if args.end is not None:
        end = datetime.datetime.strptime(args.end, TIME_FORMAT).time()
    else:
        end = None
    place = args.place
    is_public = True
    if args.private:
        is_public = False

    controller.create_event(name, date, begin, end, place, is_public)


def edit_event(args, controller):
    if args.private and args.public:
        raise ValueError("Arguments private and public aren't allowed together")

    event_id = args.id
    name = args.name
    if args.date is not None:
        date = datetime.datetime.strptime(args.date, DATE_FORMAT).date()
    else:
        date = None
    if args.begin is not None:
        begin = datetime.datetime.strptime(args.begin, TIME_FORMAT).time()
    else:
        begin = None
    if args.end is not None:
        end = datetime.datetime.strptime(args.end, TIME_FORMAT).time()
    else:
        end = None
    place = args.place
    if args.public:
        is_public = True
    elif args.private:
        is_public = False
    else:
        is_public = None

    controller.edit_event(event_id, name, date, begin, end, place, is_public)


def invite_user_to_event(args, controller):
    event_id = args.id
    user_name = args.name
    controller.invite_user_to_event(event_id, user_name)


def remove_event_member(args, controller):
    event_id = args.id
    user_name = args.name
    controller.remove_event_member(event_id, user_name)


def remove_event(args, controller):
    event_id = args.id
    controller.remove_event(event_id)


def create_task(args, controller):
    name = args.name
    if args.deadline is not None:
        deadline = datetime.datetime.strptime(args.deadline, DATE_FORMAT).date()
    else:
        deadline = None
    comment = args.comment
    task_type = args.type
    status = args.status
    priority = args.prior
    list_name = args.list
    parent_id = args.parent
    controller.create_task(name, deadline, comment, task_type, status, priority, list_name, parent_id)


def print_task_with_subtasks(tab_size, task):
    print("\t" * tab_size, "{} {}".format(task.id, task.name))
    for sub_task in task.subtasks:
        print_task_with_subtasks(tab_size+1, sub_task)


def print_task_tree(args, controller):
    task = controller.find_task_by_id(args.id)
    print_task_with_subtasks(0, task)


def find_task_by_params(args, controller):
    if args.id is None:
        task_id = None
    else:
        task_id = int(args.id)

    if args.deadline is not None:
        deadline = datetime.datetime.strptime(args.deadline, DATE_FORMAT)
        deadline = deadline.date()
    else:
        deadline = None

    if args.begin is not None:
        begin = datetime.datetime.strptime(args.begin, DATE_FORMAT)
        begin = begin.date()
    else:
        begin = None

    if args.end is not None:
        end = datetime.datetime.strptime(args.end, DATE_FORMAT)
        end = end.date()
    else:
        end = None

    tasks = controller.find_task_by_params(task_id, args.name, deadline, begin, end,
                                           args.type, args.status, args.prior, args.parent)
    for task in tasks:
        print(task)


def edit_task(args, controller):
    if args.deadline is not None:
        deadline = datetime.datetime.strptime(args.deadline, DATE_FORMAT).date()
    else:
        deadline = None
    controller.edit_task_status(args.id, args.status)
    controller.edit_task(args.id, args.name, deadline, args.comment, args.type, args.prior)


def add_task_executor(args, controller):
    task_id = args.id
    user_name = args.name
    controller.add_task_executor(task_id, user_name)


def remove_task_executor(args, controller):
    task_id = args.id
    user_name = args.name
    controller.remove_task_executor(task_id, user_name)


def remove_task(args, controller):
    task_id = args.id
    controller.remove_task(task_id)


def parse_plan_period(period):
    """Parse plan period from string"""

    if period == "hour":
        period = Period.EVERY_HOUR
    elif period == "day":
        period = Period.EVERY_DAY
    elif period == "week":
        period = Period.EVERY_WEEK
    elif period == "month":
        period = Period.EVERY_MONTH
    elif period == "year":
        period = Period.EVERY_YEAR
    else:
        raise ValueError("Period is not correct")
    return period


def create_plan(args, controller):
    activity_id = args.id
    period = parse_plan_period(args.period)
    interval = args.interval
    if args.action:
        controller.create_action_plan(activity_id, period, interval)
    elif args.task:
        controller.create_task_plan(activity_id, period, interval)
    elif args.event:
        controller.create_event_plan(activity_id, period, interval)


def disable_plan(args, controller):
    plan_id = int(args.id)
    controller.disable_plan(plan_id)


def enable_plan(args, controller):
    plan_id = int(args.id)
    controller.enable_plan(plan_id)


def edit_plan_interval(args, controller):
    plan_id = int(args.id)
    period = parse_plan_period(args.period)
    interval = args.interval
    controller.edit_plan_interval(plan_id, period, interval)


def remove_plan(args, controller):
    plan_id = int(args.id)
    controller.remove_plan(plan_id)


def get_all_plans(args, controller):
    plans = controller.get_all_plans()
    for p in plans:
        print(p)

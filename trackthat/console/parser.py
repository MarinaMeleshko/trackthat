"""Module provides command line arguments parsers"""

import argparse
import sys
import traceback

import trackthat.config as config
import trackthat.library.logger as logger
import trackthat.console.command_line_handlers as handlers
from trackthat.console.authorisation_controller import AuthorisationController


class HelpParser(argparse.ArgumentParser):
    """Class that inherits default error message"""

    def error(self, message):
        print('error: %s\n' % message)
        self.print_help()
        sys.exit(2)


def parse_args():
    """Create base command line parser,subparsers and parse args"""

    arg_parser = HelpParser(prog="trackthat")
    subparsers = arg_parser.add_subparsers()

    def print_main_help(*args):
        return arg_parser.print_help()

    user = subparsers.add_parser("user", help="Manage users")
    user_parser = user.add_subparsers()

    def print_user_help(*args):
        return user.print_help()

    user.set_defaults(func=print_user_help)
    create_user_parser(user_parser)

    get_current_user_parser = subparsers.add_parser("whoami", help="Get info about yourself")
    get_current_user_parser.set_defaults(func=handlers.get_current_user)

    get_date_parser = subparsers.add_parser("date", help="Get info about activities and notification on date (today "
                                                         "default)")
    get_date_parser.add_argument("--date", help="Date in string format " + config.DATE_FORMAT)
    get_date_parser.add_argument("-d", "--day", help="Date of current or other month")
    get_date_parser.add_argument("-m", "--month", help="Month of current or other year")
    get_date_parser.add_argument("-y", "--year", help="Year")
    get_date_parser.set_defaults(func=handlers.get_date)

    lists = subparsers.add_parser("list", help="Manage user lists")
    lists_parser = lists.add_subparsers()

    def print_lists_help(*args):
        return lists.print_help()

    lists.set_defaults(func=print_lists_help)
    create_list_parser(lists_parser)

    action = subparsers.add_parser("action", help="Manage actions")
    action_parser = action.add_subparsers()

    def print_action_help(*args):
        return action.print_help()

    action.set_defaults(func=print_action_help)
    create_action_parser(action_parser)

    event = subparsers.add_parser("event", help="Manage events")
    event_parser = event.add_subparsers()

    def print_event_help(*args):
        return event.print_help()

    event.set_defaults(func=print_event_help)
    create_event_parser(event_parser)

    task = subparsers.add_parser("task", help="Manage tasks")
    task_parser = task.add_subparsers()

    def print_task_help(*args):
        return task.print_help()

    task.set_defaults(func=print_task_help)
    create_task_parser(task_parser)

    plan = subparsers.add_parser("plan", help="Manage repeating plans")
    plan_parser = plan.add_subparsers()

    def print_plan_help(*args):
        return plan.print_help()

    plan.set_defaults(func=print_plan_help)
    create_plan_parser(plan_parser)

    arg_parser.set_defaults(func=print_main_help)

    args = arg_parser.parse_args()
    return args


def create_user_parser(parser):
    change = parser.add_parser("change", help="Log out and authorize as other user")
    change.add_argument("username", help="New username")
    change.set_defaults(func=handlers.change_user)

    add = parser.add_parser("add", help="Add new user")
    add.add_argument("username", help="New user name")
    add.set_defaults(func=handlers.add_user)

    all = parser.add_parser("all", help="Show all users")
    all.set_defaults(func=handlers.get_all_users)


def create_list_parser(parser):
    all = parser.add_parser("all", help="Get action and task lists")
    all.add_argument("-a", "--action", help="Only action lists", action="store_true")
    all.add_argument("-t", "--task", help="Only task lists", action="store_true")
    all.set_defaults(func=handlers.get_lists)

    show = parser.add_parser("show", help="Show list content")
    show.add_argument("listname", help="List name")
    show.set_defaults(func=handlers.show_list)

    add = parser.add_parser("add", help="Add new list")
    add.add_argument("name", help="List name")
    add.add_argument("-a", "--action", help="Action list", action="store_true")
    add.add_argument("-t", "--task", help="Task list", action="store_true")
    add.set_defaults(func=handlers.create_list)

    remove = parser.add_parser("remove", help="Delete list")
    remove.add_argument("name", help="List name")
    remove.set_defaults(func=handlers.remove_list)

    rename = parser.add_parser("rename", help="Rename list")
    rename.add_argument("oldname", help="Old name")
    rename.add_argument("newname", help="New name")
    rename.set_defaults(func=handlers.rename_list)

    add_user = parser.add_parser("adduser", help="Add user to task list")
    add_user.add_argument("user", help="User name")
    add_user.add_argument("list", help="List name")
    add_user.set_defaults(func=handlers.add_user_to_list)

    remove_user = parser.add_parser("deluser", help="Remove user from task list")
    remove_user.add_argument("user", help="User name")
    remove_user.add_argument("list", help="List name")
    remove_user.set_defaults(func=handlers.remove_user_from_list)


def create_action_parser(parser):
    add = parser.add_parser("add", help="Create new action")
    add.add_argument("-n", "--name", help="Name")
    add.add_argument("-c", "--comment", help="Comment")
    add.add_argument("-d", "--deadline", help="Deadline in format " + config.DATE_FORMAT)
    add.add_argument("-l", "--list", help="List name")
    add.set_defaults(func=handlers.create_action)

    edit = parser.add_parser("edit", help="Edit action")
    edit.add_argument("id", help="action ID")
    edit.add_argument("-n", "--name", help="New name")
    edit.add_argument("-c", "--comment", help="New comment")
    edit.add_argument("-d", "--deadline", help="New deadline in format " + config.DATE_FORMAT)
    edit.set_defaults(func=handlers.edit_action)

    remove = parser.add_parser("remove", help="Remove action")
    remove.add_argument("id", help="action ID")
    remove.set_defaults(func=handlers.remove_action)


def create_event_parser(parser):
    add = parser.add_parser("add", help="Create new event")
    add.add_argument("-n", "--name", help="Name")
    add.add_argument("--date", help="Date")
    add.add_argument("-b", "--begin", help="Begin time")
    add.add_argument("-e", "--end", help="End time")
    add.add_argument("-p", "--place", help="Place")
    add.add_argument("-pu", "--public", help="Public event", action="store_true")
    add.add_argument("-pr", "--private", help="Private event", action="store_true")
    add.set_defaults(func=handlers.create_event)

    edit = parser.add_parser("edit", help="Edit event")
    edit.add_argument("id", help="event ID")
    edit.add_argument("-n", "--name", help="Name")
    edit.add_argument("--date", help="Date")
    edit.add_argument("-b", "--begin", help="Begin time")
    edit.add_argument("-e", "--end", help="End time")
    edit.add_argument("-p", "--place", help="Place")
    edit.add_argument("-pu", "--public", help="Public event", action="store_true")
    edit.add_argument("-pr", "--private", help="Private event", action="store_true")
    edit.set_defaults(func=handlers.edit_event)

    remove = parser.add_parser("remove", help="Remove event")
    remove.add_argument("id", help="event ID")
    remove.set_defaults(func=handlers.remove_event)

    invite = parser.add_parser("invite", help="Invite user")
    invite.add_argument("id", help="event ID")
    invite.add_argument("name", help="User name")
    invite.set_defaults(func=handlers.invite_user_to_event)

    leave = parser.add_parser("leave", help="Remove user from members or leave event")
    leave.add_argument("id", help="event ID")
    leave.add_argument("-n", "--name", help="User name. Your name by default")
    leave.set_defaults(func=handlers.remove_event_member)


def create_task_parser(parser):
    add = parser.add_parser("add", help="Create new task")
    add.add_argument("-n", "--name", help="Name")
    add.add_argument("-d", "--deadline", help="Deadline")
    add.add_argument("-c", "--comment", help="Comment")
    add.add_argument("-t", "--type", help="Task type")
    add.add_argument("-s", "--status", help="Status")
    add.add_argument("-p", "--prior", help="Priority")
    add.add_argument("-l", "--list", help="List name")
    add.add_argument("-pa", "--parent", help="Parent task ID")
    add.set_defaults(func=handlers.create_task)

    find = parser.add_parser("find", help="Find tasks by specific params")
    find.add_argument("-id", "--id", help="Task ID")
    find.add_argument("-n", "--name", help="Name")
    find.add_argument("-d", "--deadline", help="Deadline")
    find.add_argument("-b", "--begin", help="Find tasks with deadline later")
    find.add_argument("-e", "--end", help="Find tasks with deadline earlier")
    find.add_argument("-t", "--type", help="Task type")
    find.add_argument("-s", "--status", help="Status")
    find.add_argument("-p", "--prior", help="Priority")
    find.add_argument("-pa", "--parent", help="Parent task ID")
    find.set_defaults(func=handlers.find_task_by_params)

    tree = parser.add_parser("tree", help="Print subtasks tree")
    tree.add_argument("id", help="Task ID")
    tree.set_defaults(func=handlers.print_task_tree)

    edit = parser.add_parser("edit", help="Edit task")
    edit.add_argument("id", help="Task ID")
    edit.add_argument("-n", "--name", help="Name")
    edit.add_argument("-d", "--deadline", help="Deadline")
    edit.add_argument("-c", "--comment", help="Comment")
    edit.add_argument("-t", "--type", help="Task type")
    edit.add_argument("-s", "--status", help="Status")
    edit.add_argument("-p", "--prior", help="Priority")
    edit.set_defaults(func=handlers.edit_task)

    remove = parser.add_parser("remove", help="Remove task")
    remove.add_argument("id", help="Task ID")
    remove.set_defaults(func=handlers.remove_task)

    add_executor = parser.add_parser("addexec", help="Add task executor")
    add_executor.add_argument("id", help="Task ID")
    add_executor.add_argument("name", help="User name")
    add_executor.set_defaults(func=handlers.add_task_executor)

    remove_executor = parser.add_parser("removeexec", help="Remove task executor")
    remove_executor.add_argument("id", help="Task ID")
    remove_executor.add_argument("-n", "--name", help="User name")
    remove_executor.set_defaults(func=handlers.remove_task_executor)


def create_plan_parser(parser):
    add = parser.add_parser("add", help="Create new plan")
    add.add_argument("id", help="Activity ID")
    add.add_argument("period", help="Period: hour, day, week, month, year")
    add.add_argument("-i", "--interval", help="Periods count before next repeating", default=1)
    types = add.add_mutually_exclusive_group()
    types.add_argument("-a", "--action", help="Action", action="store_true")
    types.add_argument("-t", "--task", help="Task", action="store_true")
    types.add_argument("-e", "--event", help="Event", action="store_true")
    add.set_defaults(func=handlers.create_plan)

    disable = parser.add_parser("disable", help="Disable plan")
    disable.add_argument("id", help="Plan ID")
    disable.set_defaults(func=handlers.disable_plan)

    enable = parser.add_parser("enable", help="Enable plan")
    enable.add_argument("id", help="Plan ID")
    enable.set_defaults(func=handlers.enable_plan)

    interval = parser.add_parser("interval", help="Edit interval")
    interval.add_argument("id", help="Plan ID")
    interval.add_argument("period", help="New period: hour, day, week, month, year")
    interval.add_argument("-i", "--interval", help="Periods count before next repeating", default=1)
    interval.set_defaults(func=handlers.edit_plan_interval)

    remove = parser.add_parser("remove", help="Remove plan")
    remove.add_argument("id", help="PLan ID")
    remove.set_defaults(func=handlers.remove_plan)

    all = parser.add_parser("all", help="Show all user plans")
    all.set_defaults(func=handlers.get_all_plans)


def main():
    logger.set_logging_settings(config.LOGGER_ENABLED,
                                config.LOGGER_FORMAT,
                                config.LOGGER_PATH,
                                config.LOGGER_LEVEL)

    controller = AuthorisationController(config.DATABASE_PATH)
    args = parse_args()
    try:
        args.func(args, controller)
    except Exception as e:
        print(e, file=sys.stderr)
        # traceback.print_exc(file=sys.stderr)


if __name__ == '__main__':
    main()

import sqlalchemy as sql

from trackthat.library.model.client import User


class AppUser(User):
    """Class that contains user authorisation logic"""

    is_authorized = sql.Column(sql.Boolean, default=False)

    __mapper_args__ = {"polymorphic_identity": "app_user"}

    def __init__(self, name="Username"):
        super().__init__(name)

"""This module provides user authorisation logic and command line handlers"""

from trackthat.console.app_user import AppUser
from trackthat.library.trackthat_api import (
    TrackThatApi,
    configure_session
)


class AuthorisationError(Exception):
    def __init__(self, message):
        self.message = message


def check_authorisation(method):
    """
    Decorator for AuthorisationController methods
    Check if user is authorised
    """
    def check(self, *args, **kwargs):
        if self.user is None:
            raise AuthorisationError("You aren't authorised")
        return method(self, *args, **kwargs)
    return check


class AuthorisationController:
    """Class provides authorisation logic and calls methods from TrackThatApi"""

    def __init__(self, data_base_path):
        self.session = configure_session(data_base_path)
        self.user = self.session.query(AppUser).filter_by(is_authorized=True).first()
        self.api = TrackThatApi(self.user, data_base_path)

    def log_in(self, user):
        if self.user is not None:
            self.user.is_authorized = False
        self.session.commit()

        new_user = self.session.query(AppUser).filter_by(name=user.name).first()
        new_user.is_authorized = True
        self.user = new_user
        self.api.user = new_user
        self.session.commit()

    def get_current_user(self):
        return self.user

    def get_all_users(self):
        return self.api.get_all_users()

    @check_authorisation
    def get_all_plans(self):
        return TrackThatApi.get_all_plans(self.user)

    def add_user(self, name):
        user = AppUser(name=name)
        return self.api.add_user(user)

    def find_user(self, name):
        return self.api.find_user(name)

    @check_authorisation
    def get_date(self, date):
        return TrackThatApi.get_date(self.user, date)

    @check_authorisation
    def get_action_lists(self):
        return self.api.get_action_lists(self.user)

    @check_authorisation
    def get_task_lists(self):
        return self.api.get_task_lists(self.user)

    @check_authorisation
    def get_list(self, name):
        return self.api.get_list(self.user, name)

    @check_authorisation
    def create_action_list(self, name):
        return self.api.create_action_list(self.user, name)

    @check_authorisation
    def create_task_list(self, name):
        return self.api.create_task_list(self.user, name)

    @check_authorisation
    def remove_list(self, name):
        self.api.remove_list(self.user, name)

    @check_authorisation
    def add_user_to_list(self, user_name, list_name):
        self.api.add_user_to_list(self.user, user_name, list_name)

    @check_authorisation
    def remove_user_from_list(self, user_name, list_name):
        self.api.remove_user_from_list(self.user, user_name, list_name)

    @check_authorisation
    def rename_list(self, old_name, new_name):
        return self.api.rename_list(self.user, old_name, new_name)

    @check_authorisation
    def create_action(self, name, deadline, comment, list_name):
        return self.api.create_action(self.user, name, deadline, comment, list_name)

    @check_authorisation
    def edit_action(self, action_id, name, deadline, comment):
        self.api.edit_action(self.user, action_id, name, deadline, comment)

    @check_authorisation
    def remove_action(self, action_id):
        self.api.remove_action(action_id)

    @check_authorisation
    def create_event(self, name, date, begin, end, place, is_public):
        return self.api.create_event(self.user, name, date, begin, end, place, is_public)

    @check_authorisation
    def edit_event(self, event_id, name, date, begin, end, place, is_public):
        self.api.edit_event(self.user, event_id, name, date, begin, end, place, is_public)

    @check_authorisation
    def remove_event(self, event_id):
        self.api.remove_event(self.user, event_id)

    @check_authorisation
    def create_task(self, name, deadline, comment, task_type, status, priority, list_name, parent_id):
        return self.api.create_task(self.user, name, deadline, comment,
                                    task_type, status, priority, list_name, parent_id)

    @check_authorisation
    def find_task_by_id(self, task_id):
        return self.api.validate_task_exists(task_id)

    @check_authorisation
    def find_task_by_params(self, task_id, name, deadline, begin, end, task_type, status, priority, parent_id):
        return self.api.find_task_by_params(self.user, task_id, name, deadline, begin, end,
                                            task_type, status, priority, parent_id)

    @check_authorisation
    def edit_task_status(self, task_id, status):
        self.api.edit_task_status(self.user, task_id, status)

    @check_authorisation
    def edit_task(self, task_id, name, deadline, comment, task_type, priority):
        self.api.edit_task(self.user, task_id, name, deadline, comment, task_type, priority)

    @check_authorisation
    def remove_task(self, task_id):
        self.api.remove_task(self.user, task_id)

    @check_authorisation
    def create_action_plan(self, action_id, period, interval):
        return self.api.create_action_plan(self.user, action_id, period, interval)

    @check_authorisation
    def create_task_plan(self, task_id, period, interval):
        return self.api.create_task_plan(self.user, task_id, period, interval)

    @check_authorisation
    def create_event_plan(self, event_id, period, interval):
        return self.api.create_event_plan(self.user, event_id, period, interval)

    @check_authorisation
    def invite_user_to_event(self, event_id, user_name):
        self.api.invite_user_to_event(self.user, event_id, user_name)

    @check_authorisation
    def remove_event_member(self, event_id, name):
        self.api.remove_event_member(self.user, event_id, name)

    @check_authorisation
    def add_task_executor(self, task_id, user_name):
        self.api.add_task_executor(self.user, task_id, user_name)

    @check_authorisation
    def remove_task_executor(self, task_id, name):
        self.api.remove_task_executor(self.user, task_id, name)

    @check_authorisation
    def disable_plan(self, plan_id):
        self.api.disable_plan(self.user, plan_id)

    @check_authorisation
    def enable_plan(self, plan_id):
        self.api.enable_plan(self.user, plan_id)

    @check_authorisation
    def remove_plan(self, plan_id):
        self.api.remove_plan(self.user, plan_id)

    @check_authorisation
    def edit_plan_interval(self, plan_id, period, interval):
        self.api.edit_plan_interval(self.user, plan_id, period, interval)

    def __del__(self):
        self.session.commit()

import os
import logging

base_directory = os.path.abspath(os.path.dirname(__file__))
DATABASE_PATH = "sqlite:///" + os.path.join(base_directory, ":database.db")
DATE_FORMAT = "%d.%m.%Y"
TIME_FORMAT = "%H:%M"
LOGGER_ENABLED = True
LOGGER_FORMAT = "%(asctime)s - %(name)s - [%(levelname)s] - %(message)s"
LOGGER_PATH = os.path.join(base_directory, "logger.log")
LOGGER_LEVEL = logging.DEBUG

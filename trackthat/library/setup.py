from setuptools import setup, find_packages
import os
import runpy


def get_version():
    filename = os.path.join(os.path.dirname(__file__), "__init__.py")
    var = runpy.run_path(filename)
    return var["__version__"]


_VERSION = get_version()

setup(
    name='TrackThatLibrary',
    version=_VERSION,
    install_requires=['sqlalchemy'],
    packages=find_packages())

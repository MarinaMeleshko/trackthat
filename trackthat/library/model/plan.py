import sqlalchemy as sql
import sqlalchemy.orm as orm
import datetime
from dateutil.relativedelta import relativedelta

from trackthat.library.model.base import Base
from trackthat.library.controllers.user_controller import (
    create_task,
    create_event
)


class Period:
    """Enum for storing periodicity """

    EVERY_HOUR = "hour"
    EVERY_DAY = "day"
    EVERY_WEEK = "week"
    EVERY_MONTH = "month"
    EVERY_YEAR = "year"


class Plan(Base):
    """Class that provides activity or notification repeating"""

    __tablename__ = "plan"
    id = sql.Column(sql.Integer, primary_key=True)
    is_enabled = sql.Column(sql.Boolean)
    period = sql.Column(sql.String)
    interval = sql.Column(sql.Integer)
    last_activated = sql.Column(sql.Date)
    type = sql.Column(sql.String)

    __mapper_args__ = {"polymorphic_on": type}

    def __init__(self, period=Period.EVERY_DAY, interval=1):
        self.is_enabled = True
        self.period = period
        self.interval = interval
        self.update()

    def update(self):
        """
        Set actual activity state
        This method set last_activated at next time
        E.g. user wants to plan event every Saturday
        If update was called on Wednesday last_activated is next Saturday
        """

        if self.is_enabled:

            if self.period == Period.EVERY_HOUR:
                delta = relativedelta(hours=+self.interval)
            if self.period == Period.EVERY_DAY:
                delta = relativedelta(days=+self.interval)
            if self.period == Period.EVERY_WEEK:
                    delta = relativedelta(weeks=+self.interval)
            if self.period == Period.EVERY_MONTH:
                delta = relativedelta(months=+self.interval)
            if self.period == Period.EVERY_YEAR:
                delta = relativedelta(years=+self.interval)

            last_activated = self.last_activated
            while last_activated < datetime.date.today():
                last_activated += delta
            self.last_activated = last_activated
        return self.last_activated

    def __str__(self):
        return "ID: {} interval: {}".format(
            self.id,
            datetime.timedelta(seconds=self.interval)
        )


class ActionPlan(Plan):
    """Plan to repeat actions"""

    action_id = sql.Column(sql.Integer, sql.ForeignKey("action.id"))
    action = orm.relationship("Action", lazy="joined", back_populates="plan")

    __mapper_args__ = {"polymorphic_identity": "action_plan"}

    def __init__(self, action, periodicity=Period.EVERY_DAY, interval=1):
        self.action = action
        self.last_activated = action.deadline
        super().__init__(periodicity, interval)

    def update(self, change_action_deadline=None):
        last_activated = super().update()
        if change_action_deadline is None:
            self.action.deadline = last_activated
        else:
            pass  # think about it

    def __str__(self):
        return super().__str__() + "\n{}\n".format(self.action)


class EventPlan(Plan):
    """Plan for repeating events"""

    event_id = sql.Column(sql.Integer, sql.ForeignKey("event.id"))
    event = orm.relationship("Event", lazy="joined", back_populates="plan")

    __mapper_args__ = {"polymorphic_identity": "event_plan"}

    def __init__(self, event, periodicity=Period.EVERY_DAY, interval=1):
        self.event = event
        self.last_activated = event.date
        super().__init__(periodicity, interval)

    def update(self, change_event_date=None):
        last_activated = super().update()
        if last_activated == self.event.date:
            return
        new_event = create_event(
            self.event.creator,
            self.event.name,
            last_activated,
            self.event.begin_time,
            self.event.end_time,
            self.event.place,
            self.event.is_public
        )
        from trackthat.library.controllers.activity_controller import invite_user_to_event
        for user in self.event.members:
            invite_user_to_event(self.event, new_event.creator, user)
        self.event = new_event
        return new_event

    def __str__(self):
        return super().__str__() + "\n{}\n".format(self.event)


class TaskPlan(Plan):
    """Plan for repeating tasks"""

    task_id = sql.Column(sql.Integer, sql.ForeignKey("task.id"))
    task = orm.relationship("Task", lazy="joined", back_populates="plan")

    __mapper_args__ = {"polymorphic_identity": "task_plan"}

    def __init__(self, task, periodicity=Period.EVERY_DAY, interval=1):
        self.task = task
        self.last_activated = task.deadline
        super().__init__(periodicity, interval)

    def update(self):
        last_activated = super().update()
        if last_activated == self.task.deadline:
            return
        list_name = next(l.name for l in self.task.creator.activities if self.task in l.content)
        new_task = create_task(
            self.task.creator,
            self.task.name,
            last_activated,
            self.task.comment,
            self.task.type,
            self.task.status,
            self.task.priority,
            list_name,
            self.task.parent
        )
        from trackthat.library.controllers.activity_controller import add_task_executor
        for executor in self.task.executors:
            add_task_executor(new_task, new_task.creator, executor)
        self.task = new_task
        return new_task

    def __str__(self):
        return super().__str__() + "\n{}\n".format(self.task)


class NotificationPlan(Plan):
    """Plan for notifications repeating"""

    notification_id = sql.Column(sql.Integer, sql.ForeignKey("notification.id"))
    notification = orm.relationship("Notification", lazy="joined", back_populates="plan")

    __mapper_args__ = {"polymorphic_identity": "notification_plan"}

    def __init__(self, notification, periodicity, interval):
        self.notification = notification
        self.last_activated = datetime.datetime(year=notification.date.year,
                                                month=notification.date.month,
                                                day=notification.date.day,
                                                hour=notification.time.hour,
                                                minute=notification.time.minute,
                                                second=notification.time.second)
        super().__init__(periodicity, interval)

    def update(self):
        """
        Notification has date and time, and interval can be less than one day
        Time and date should be updated
        """
        if self.is_enabled:

            if self.period == Period.EVERY_HOUR:
                delta = relativedelta(hours=+self.interval)
            if self.period == Period.EVERY_DAY:
                delta = relativedelta(days=+self.interval)
            if self.period == Period.EVERY_WEEK:
                    delta = relativedelta(weeks=+self.interval)
            if self.period == Period.EVERY_MONTH:
                delta = relativedelta(months=+self.interval)
            if self.period == Period.EVERY_YEAR:
                delta = relativedelta(years=+self.interval)

            last_activated = self.last_activated
            while last_activated < datetime.datetime.now():
                last_activated += delta
            self.last_activated = last_activated

            self.notification.date = last_activated.date()
            self.notification.time = last_activated.time()

    def __str__(self):
        return super().__str__() + "\n{}\n".format(self.notification)

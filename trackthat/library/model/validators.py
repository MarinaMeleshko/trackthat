"""This module provides datetime validations"""

import datetime


def validate_date(date):
    """Validate that date is later than now. Raise ValueError otherwise"""

    if date is not None and date < datetime.date.today():
        raise ValueError("Date can't be earlier than today")


def validate_begin_time_earlier_end_time(begin_time, end_time):
    """Validate that end time is later than begin_time"""

    if end_time <= begin_time:
        raise ValueError("End time can't be earlier than begin time")


def validate_plan_interval(interval):
    """Validate that time interval isn't less one hour"""

    if interval < 3600:
        raise ValueError("Too short interval")


def validate_date_begin_end_time(date, begin, end):
    """Validate that date is later than now, end time is later than begin_time"""

    validate_date(date)
    if (begin is not None and
            date == datetime.date.today() and
            begin < ((datetime.datetime.now()).time()).replace(microsecond=0)):
        raise ValueError("Time can not be earlier than now")
    if begin is None and end is not None:
        raise ValueError("Begin time is undefined")
    if begin is not None and end is not None:
        validate_begin_time_earlier_end_time(begin, end)

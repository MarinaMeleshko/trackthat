import datetime
import sqlalchemy as sql
import sqlalchemy.orm as orm

from trackthat.library.model.base import Base


class Action(Base):
    """Class that stores user actions"""

    __tablename__ = "action"
    id = sql.Column(sql.Integer, primary_key=True)
    name = sql.Column(sql.String)
    deadline = sql.Column(sql.Date, nullable=True)
    comment = sql.Column(sql.String, nullable=True)
    date_id = sql.Column(sql.Integer, sql.ForeignKey("date.id"))
    plan = orm.relationship("ActionPlan", uselist=False, back_populates="action")

    def __init__(self, name="Action", deadline=None, comment=None):
        self.name = name
        self.deadline = deadline
        self.comment = comment
        self.plan = None

    def __str__(self):
        return "ACTION ID: {} name: {}\ncomment: {}\ndeadline: {}".format(
            self.id,
            self.name,
            self.comment,
            self.deadline
        )


EventUserAssociation = sql.Table("event_user", Base.metadata,
                                 sql.Column("event_id", sql.Integer, sql.ForeignKey("event.id")),
                                 sql.Column("user_id", sql.Integer, sql.ForeignKey("user.id")))


class Event(Base):
    """Class for storing user events"""

    __tablename__ = "event"
    id = sql.Column(sql.Integer, primary_key=True)
    creator_id = sql.Column(sql.Integer, sql.ForeignKey("user.id"))
    creator = orm.relationship("User", lazy="joined")
    members = orm.relationship("User", lazy="joined", secondary=EventUserAssociation)
    name = sql.Column(sql.String)
    date = sql.Column(sql.Date)
    begin_time = sql.Column(sql.Time, nullable=True)
    end_time = sql.Column(sql.Time, nullable=True)
    place = sql.Column(sql.String, nullable=True)
    is_public = sql.Column(sql.Boolean)
    plan = orm.relationship("EventPlan", uselist=False, back_populates="event")

    def __init__(self, creator, name="Event",
                 date=datetime.date.today(),
                 begin_time=None,
                 end_time=None,
                 place=None,
                 is_public=True):
        self.creator = creator
        self.name = name
        self.date = date
        self.begin_time = begin_time
        self.end_time = end_time
        self.place = place
        self.is_public = is_public
        self.members = []
        if creator is not None:
            self.members.append(creator)
        self.plan = None

    def invite_user(self, user):
        if user not in self.members:
            self.members.append(user)

    def remove_user(self, user):
        try:
            self.members.remove(user)
        except ValueError:
            pass

    def __str__(self):
        members = "\n".join(list(map(lambda u: str(u), self.members)))
        if self.is_public:
            publicity = "public"
        else:
            publicity = "private"
        return ("EVENT ID: {} name:{}\ndate: {}\nbegin: {} end: {}\nplace: "
                "{}\npublicity: {}\ncreator:{}\nmembers:\n{}\n".format(
                    self.id,
                    self.name,
                    self.date,
                    self.begin_time,
                    self.end_time,
                    self.place,
                    publicity,
                    self.creator,
                    members))


class TaskPriority:
    """Enum for storing task priority values"""

    LOW = 0
    MEDIUM = 1
    HEIGHT = 2
    MAXIMAL = 3


class TaskStatus:
    """
    Enum for storing task status values
    NEW - task is created just now
    IN_PROCESS - someone executes task
    EXECUTED - task executing is completed
    CLOSED - task is closed
    FAILED - task executing is not successful
    CANCELED - task is not actual
    """

    NEW = "new"
    IN_PROCESS = "in process"
    EXECUTED = "executed"
    CLOSED = "closed"
    FAILED = "failed"
    CANCELED = "canceled"


TaskUserAssociation = sql.Table("task_user", Base.metadata,
                                sql.Column("task_id", sql.Integer, sql.ForeignKey("task.id")),
                                sql.Column("user_id", sql.Integer, sql.ForeignKey("user.id")))


class Task(Base):
    """Class to store user tasks"""

    __tablename__ = "task"
    id = sql.Column(sql.Integer, primary_key=True)
    creator_id = sql.Column(sql.Integer, sql.ForeignKey("user.id"))
    creator = orm.relationship("User", lazy="joined")
    executors = orm.relationship("User", lazy="joined", secondary=TaskUserAssociation)
    name = sql.Column(sql.String)
    parent_task_id = sql.Column(sql.Integer, sql.ForeignKey("task.id"))
    parent = orm.relationship("Task", remote_side=[id], backref="subtasks", lazy="joined")
    deadline = sql.Column(sql.Date, nullable=True)
    comment = sql.Column(sql.String, nullable=True)
    type = sql.Column(sql.String, nullable=True)
    status = sql.Column(sql.String, nullable=True)
    priority = sql.Column(sql.String, nullable=True)
    plan = orm.relationship("TaskPlan", uselist=False, back_populates="task")

    def __init__(self,
                 creator,
                 name="Task",
                 deadline=None,
                 comment=None,
                 task_type=None,
                 status=TaskStatus.NEW,
                 priority=TaskPriority.MEDIUM):
        self.creator = creator
        self.executors = []
        self.name = name
        self.parent_task = None
        self.subtasks = []
        self.deadline = deadline
        self.comment = comment
        self.type = task_type
        self.status = status
        self.priority = priority
        self.plan = None

    def add_executor(self, executor):
        if executor not in self.executors:
            self.executors.append(executor)

    def remove_executor(self, executor):
        try:
            self.executors.remove(executor)
        except ValueError:
            pass

    def add_subtask(self, subtask):
        subtask.parent_task = self
        self.subtasks.append(subtask)
        return subtask

    def close(self):
        self.status = TaskStatus.CLOSED

        """Close task when all subtasks are closed"""
        if self.parent_task is not None:
            if all(t.status == TaskStatus.CLOSED for t in self.parent_task.subtasks):
                self.parent_task.close()

    def __str__(self):
        executors = "\n".join(list(map(lambda e: str(e), self.executors)))
        subtasks = "\n".join(list(map(lambda sub_task:
                                      "ID: {} name {}".format(sub_task.id, sub_task.name), self.subtasks)))
        return ("TASK ID: {} name: {}\ncomment: {}\ndeadline: {}\ntype: {}\nstatus: {}\n"
                "priority: {}\ncreator: {}\nexecutors:\n{}\nsubtasks:\n{}\n".format(
                    self.id,
                    self.name,
                    self.comment,
                    self.deadline,
                    self.type,
                    self.status,
                    self.priority,
                    self.creator,
                    executors,
                    subtasks
                ))

import datetime
import sqlalchemy as sql
import sqlalchemy.orm as orm

from trackthat.library.model.base import Base
import trackthat.library.model.activities as activities


class Calendar(Base):
    """Class to manage user calendar"""

    __tablename__ = "calendar"
    id = sql.Column(sql.Integer, primary_key=True)
    dates = orm.relationship("Date", lazy="joined")
    user_id = sql.Column(sql.Integer, sql.ForeignKey("user.id"))

    def __init__(self):
        self.dates = []

    def add_date(self, date):
        if all(date != d.date for d in self.dates):
            self.dates.append(date)

    def get_date(self, date=datetime.date.today()):
        return next((d for d in self.dates if d.date == date), None)


DateEventAssociation = sql.Table("date_event", Base.metadata,
                                 sql.Column("date_id", sql.Integer, sql.ForeignKey("date.id")),
                                 sql.Column("event_id", sql.Integer, sql.ForeignKey("event.id")))

DateTaskAssociation = sql.Table("date_task", Base.metadata,
                                sql.Column("date_id", sql.Integer, sql.ForeignKey("date.id")),
                                sql.Column("task_id", sql.Integer, sql.ForeignKey("task.id")))


class Date(Base):
    """Class for manage activities with deadline or date"""

    __tablename__ = "date"
    id = sql.Column(sql.Integer, primary_key=True)
    date = sql.Column(sql.Date)
    calendar_id = sql.Column(sql.Integer, sql.ForeignKey("calendar.id"))
    actions = orm.relationship("Action", lazy="joined")
    events = orm.relationship("Event", lazy="joined", secondary=DateEventAssociation)
    tasks = orm.relationship("Task", lazy="joined", secondary=DateTaskAssociation)
    notifications = orm.relationship("Notification", lazy="joined")

    def __init__(self, date=datetime.date.today()):
        self.date = date
        self.actions = []
        self.events = []
        self.tasks = []
        self.notifications = []

    @property
    def activities(self):
        return self.actions + self.events + sorted(self.tasks,
                                                   key=lambda task: task.priority, reverse=True)

    def add_activity(self, activity):
        if isinstance(activity, activities.Task):
            self.tasks.append(activity)
        elif isinstance(activity, activities.Event):
            self.events.append(activity)
        elif isinstance(activity, activities.Action):
            self.actions.append(activity)

    def add_notification(self, notification):
        self.notifications.append(notification)

    def remove_activity(self, activity):
        try:
            if isinstance(activity, activities.Task):
                self.tasks.remove(activity)
            elif isinstance(activity, activities.Event):
                self.events.remove(activity)
            elif isinstance(activity, activities.Action):
                self.actions.remove(activity)
        except ValueError:
            pass

    def remove_notification(self, notification):
        try:
            self.notifications.remove(notification)
        except ValueError:
            pass

    def __str__(self):
        all_activities = "\n".join(list(map(lambda a: str(a), self.activities)))
        all_notifications = "\n".join(list(map(lambda n: str(n), self.notifications)))
        return "\n".join([str(self.date), all_activities, all_notifications])


class Notification(Base):
    """Class for storing user notifications"""

    __tablename__ = "notification"
    id = sql.Column(sql.Integer, primary_key=True)
    name = sql.Column(sql.String)
    comment = sql.Column(sql.String, nullable=True)
    date = sql.Column(sql.Date)
    time = sql.Column(sql.Time, nullable=True)
    date_id = sql.Column(sql.Integer, sql.ForeignKey("date.id"))
    plan = orm.relationship("NotificationPlan", uselist=False, back_populates="notification")

    def __init__(self, name="Notification", date=datetime.date.today(), time=None):
        self.name = name
        self.comment = None
        self.date = date
        self.time = time
        self.plan = None

    def __str__(self):
        if self.comment is not None:
            comment = self.comment
        else:
            comment = ""
        if self.time is not None:
            time = str(self.time)
        else:
            time = ""
        return " ".join(["ID:", str(self.id), "Notification:", time, str(self.date), self.name, comment])

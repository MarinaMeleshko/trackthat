import sqlalchemy as sql
import sqlalchemy.orm as orm

import trackthat.library.model.calendar as calendar
from trackthat.library.model.base import Base

UserPlanAssociation = sql.Table("user_plan", Base.metadata,
                                sql.Column("user_id", sql.Integer, sql.ForeignKey("user.id")),
                                sql.Column("plan_id", sql.Integer, sql.ForeignKey("plan.id")))


class User(Base):
    __tablename__ = "user"
    id = sql.Column(sql.Integer, primary_key=True)
    name = sql.Column(sql.String)
    calendar = orm.relationship("Calendar", uselist=False, lazy="joined")
    activities = orm.relationship("ActivityContainer", lazy="joined")
    plans = orm.relationship("Plan", secondary=UserPlanAssociation)
    type = sql.Column(sql.String)

    __mapper_args__ = {'polymorphic_on': type}

    def __init__(self, name="Username"):
        self.name = name
        self.calendar = calendar.Calendar()

        """Action and task lists"""
        self.activities = []

        """User has action list and task list, actions and tasks can't exist outside lists"""
        self.create_action_list()
        self.create_task_list()
        self.plans = []

    def create_action_list(self, name="Action list"):
        new_list = ActionList(name)
        self.activities.append(new_list)
        return new_list

    def create_task_list(self, name="Task list"):
        new_list = TaskList(self, name)
        self.activities.append(new_list)
        return new_list

    def create_folder(self, name="Folder"):
        folder = Folder(name)
        self.activities.append(folder)
        return folder

    def remove_container(self, container):
        try:
            self.activities.remove(container)
        except ValueError:
            pass

    def __str__(self):
        return " ".join(["ID:", str(self.id), self.name])


class ActivityContainer(Base):
    """Class that stores list or folder"""

    __tablename__ = "activity_container"
    id = sql.Column(sql.Integer, primary_key=True)
    name = sql.Column(sql.String)
    type = sql.Column(sql.String)
    creator_id = sql.Column(sql.Integer, sql.ForeignKey("user.id"))

    __mapper_args__ = {"polymorphic_on": type}

    def __init__(self, name):
        self.content = []
        self.name = name

    def add(self, activity):
        self.content.append(activity)

    def remove(self, activity):
        try:
            self.content.remove(activity)
        except ValueError:
            pass

    def __str__(self):
        content = "\n".join(list(map(lambda a: str(a), self.content)))
        return "{}\n{}".format(self.name, content)


ListActionAssociation = sql.Table("action_list_action", Base.metadata,
                                  sql.Column("action_list_id", sql.Integer, sql.ForeignKey("activity_container.id")),
                                  sql.Column("action_id", sql.Integer, sql.ForeignKey("action.id")))


class ActionList(ActivityContainer):
    """List only for actions"""

    content = orm.relationship("Action", secondary=ListActionAssociation, lazy="joined")

    __mapper_args__ = {"polymorphic_identity": "action_list"}

    def __init__(self, name="Action list"):
        super().__init__(name)


ListTaskAssociation = sql.Table("task_list_task", Base.metadata,
                                sql.Column("task_list_id", sql.Integer, sql.ForeignKey("activity_container.id")),
                                sql.Column("task_id", sql.Integer, sql.ForeignKey("task.id")))

ListUserAssociation = sql.Table("task_list_user", Base.metadata,
                                sql.Column("task_list_id", sql.Integer, sql.ForeignKey("activity_container.id")),
                                sql.Column("user_id", sql.Integer, sql.ForeignKey("user.id")))


class TaskList(ActivityContainer):
    """List only for tasks"""

    content = orm.relationship("Task", secondary=ListTaskAssociation, lazy="joined")
    creator = orm.relationship("User", lazy="joined")
    users = orm.relationship("User", secondary=ListUserAssociation, lazy="joined")

    __mapper_args__ = {"polymorphic_identity": "task_list"}

    def __init__(self, creator, name="Task list"):
        self.creator = creator
        self.users = []
        super().__init__(name)

    """Many users can have access to task list"""

    def add_user(self, user):
        if user not in self.users:
            self.users.append(user)

    def remove_user(self, user):
        try:
            self.users.remove(user)
        except ValueError:
            pass

    def __str__(self):
        users = "\n".join(list(map(lambda u: str(u), self.users)))
        content = sorted(self.content, key=lambda task: task.priority, reverse=True)
        content = "\n".join(list(map(lambda a: str(a), content)))
        return "{} creator:{}\nusers:\n{}\nCONTENT:\n{}\n".format(self.name, self.creator, users, content)


class Folder(ActivityContainer):
    """Directory for storing lists and other folders"""

    # content = orm.relationship("ActivityContainer")

    __mapper_args__ = {"polymorphic_identity": "folder"}

    def __init(self, name="Folder"):
        super().__init__(name)

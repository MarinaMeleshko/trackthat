import logging


def set_logging_settings(logger_enabled=True,
                         logger_format="%(asctime)s - %(name)s - [%(levelname)s] - %(message)s",
                         logger_path="./logger.log",
                         logger_level=logging.DEBUG):
    logger = get_logger()
    if logger_enabled:
        formatter = logging.Formatter(logger_format)
        file_handler = logging.FileHandler(logger_path)
        file_handler.setFormatter(formatter)
        logger.setLevel(logger_level)
        if logger.hasHandlers():
            logger.handlers.clear()
        logger.disabled = False
        logger.addHandler(file_handler)
    else:
        logger.disabled = True


def log(func):
    """Decorator for class methods, that write method and errors info to log"""

    def wrapper(self, *args, **kwargs):
        try:
            get_logger().info("Function: {}".format(func.__name__))
            get_logger().info("Params: {} {}".format(args, kwargs))
            result = func(self, *args, **kwargs)
            get_logger().info("Return: {}".format(result))
            return result
        except Exception as e:
            get_logger().exception(e)
            raise e from None
    return wrapper


def get_logger():
    return logging.getLogger("trackthat logger")

"""This module provides functions for all operations and saving to database"""
import datetime
from sqlalchemy.orm.attributes import flag_modified
from sqlalchemy.orm.exc import UnmappedInstanceError
import sqlalchemy
import sqlalchemy.orm as orm

from trackthat.library.model.base import Base
from trackthat.library.model import (
    activities,
    client,
    plan,
    validators
)
from trackthat.library.controllers import (
    activity_controller,
    calendar_controller,
    user_controller,
    plan_controller,
    rights_validator
)
import trackthat.library.logger as logger


def update_plan(activity_plan):
    if isinstance(activity_plan, plan.ActionPlan):
        activity_plan.update(activity_controller.change_action_deadline)
        flag_modified(activity_plan.action, "deadline")

    elif isinstance(activity_plan, plan.EventPlan) or isinstance(activity_plan, plan.TaskPlan):
        activity_plan.update()

    elif isinstance(activity_plan, plan.NotificationPlan):
        activity_plan.update()
        flag_modified(activity_plan.notification, "date")
        flag_modified(activity_plan.notification, "time")


def configure_session(data_base_path):
    session_maker = orm.sessionmaker()
    engine = sqlalchemy.create_engine("".join([data_base_path, "?check_same_thread=False"]))
    session_maker.configure(bind=engine)
    Base.metadata.create_all(engine)
    return session_maker()


class TrackThatApi:
    def __init__(self, user, data_base_path="sqlite:///:memory:"):
        self.session = configure_session(data_base_path)
        if user is not None:
            self.update_plans(user)

    def update_plans(self, user):
        for activity_plan in user.plans:
            update_plan(activity_plan)
        self.session.commit()

    @staticmethod
    def get_all_plans(user):
        return user.plans

    def get_all_users(self):
        users = self.session.query(client.User).all()
        return users

    @logger.log
    def add_user(self, user):
        self.session.add(user)
        self.session.commit()
        logger.get_logger().info("Added new user")
        return user

    def find_user(self, name):
        user = self.session.query(client.User).filter_by(name=name).first()
        return user

    def validate_user_exists(self, name):
        user = self.find_user(name)
        if user is None:
            raise ValueError("User not found")
        return user

    @staticmethod
    def get_date(user, date=datetime.date.today()):
        date_instance = calendar_controller.get_date(user.calendar, date)
        return date_instance

    @staticmethod
    def get_action_lists(user):
        lists = list(filter(lambda l: isinstance(l, client.ActionList), user.activities))
        return lists

    @staticmethod
    def get_task_lists(user):
        lists = list(filter(lambda l: isinstance(l, client.TaskList), user.activities))
        return lists

    @staticmethod
    def get_list(user, list_name):
        user_list = next((l for l in user.activities if l.name == list_name), None)
        return user_list

    @logger.log
    def create_action_list(self, user, list_name):
        action_list = user_controller.create_action_list(user, list_name)
        self.session.commit()
        logger.get_logger().info("Created action list")
        return action_list

    @logger.log
    def create_task_list(self, user, list_name):
        task_list = user_controller.create_task_list(user, list_name)
        self.session.commit()
        logger.get_logger().info("Created task list")
        return task_list

    def validate_list_exists(self, user, list_name):
        user_list = self.get_list(user, list_name)
        if user_list is None:
            raise ValueError("List with name %s doesn't exist" % list_name)
        return user_list

    @logger.log
    def remove_list(self, user, list_name):
        user_list = self.validate_list_exists(user, list_name)
        user_list = self.session.merge(user_list)

        rights_validator.validate_list_edit_remove_rights(user, user_list)

        if isinstance(user_list, client.ActionList):
            for action in user_list.content:
                self.remove_action(action.id)

        if isinstance(user_list, client.TaskList):
            for task in user_list.content:
                self.remove_task(user, task.id)

        self.session.delete(user_list)
        self.session.commit()
        logger.get_logger().info("Removed list")

    @logger.log
    def add_user_to_list(self, inviter, user_name, list_name):
        user_list = next((l for l in inviter.activities
                          if isinstance(l, client.TaskList) and l.creator is inviter), None)
        if user_list is None:
            raise ValueError("You haven't task list %s you can edit" % list_name)

        user = self.validate_user_exists(user_name)
        user_list.add_user(user)
        user.activities.append(user_list)
        logger.get_logger().info("User {} was invited to list {}".format(user, list_name))
        self.session.commit()

    @logger.log
    def remove_user_from_list(self, remover, user_name, list_name):
        user_list = next((l for l in remover.activities
                          if isinstance(l, client.TaskList) and l.creator is remover), None)
        if user_list is None:
            raise ValueError("You haven't task list %s you can edit" % list_name)

        user = self.validate_user_exists(user_name)
        user_list.remove_user(user)
        user.remove_container(user_list)
        logger.get_logger().info("User {} was removed from list {}".format(user, list_name))
        self.session.commit()

    @logger.log
    def rename_list(self, user, old_name, new_name):
        user_list = self.validate_list_exists(user, old_name)

        duplicate = self.get_list(user, new_name)
        if duplicate is not None and duplicate is not user_list:
            raise ValueError("List with name %s exists" % new_name)

        rights_validator.validate_list_edit_remove_rights(user, user_list)

        user_list.name = new_name
        self.session.commit()
        logger.get_logger().info("Renamed list")
        return user_list

    @logger.log
    def create_action(self, user, name=None, deadline=None, comment=None, list_name=None):
        if list_name is None:
            list_name = self.get_action_lists(user)[0].name
        if name is None:
            name = "Action"
        action = user_controller.create_action(user, name, deadline, comment, list_name)
        self.session.commit()
        logger.get_logger().info("Added action")
        return action

    def find_action(self, action_id):
        action = self.session.query(activities.Action).filter_by(id=action_id).first()
        return action

    def validate_action_exists(self, action_id):
        action = self.find_action(action_id)
        if action is None:
            raise ValueError("Action not found")
        return action

    @logger.log
    def edit_action(self, user, action_id, name=None, deadline=None, comment=None):
        action = self.validate_action_exists(action_id)

        if name is not None:
            action.name = name
        if comment is not None:
            action.comment = comment
        if deadline is not None:
            activity_controller.change_action_deadline(action, user, deadline)
            flag_modified(action, "deadline")
        self.session.commit()
        logger.get_logger().info("Edited action")

    @logger.log
    def remove_action(self, action_id):
        action = self.validate_action_exists(action_id)
        try:
            self.session.delete(action.plan)
        except Exception:
            pass
        self.session.delete(action)
        self.session.commit()
        logger.get_logger().info("Removed action")

    @logger.log
    def create_event(self, user,
                     name=None,
                     date=datetime.date.today(),
                     begin=None,
                     end=None,
                     place=None,
                     is_public=True):
        if name is None:
            name = "Event"
        event = user_controller.create_event(user, name, date, begin, end, place, is_public)
        self.session.commit()
        logger.get_logger().info("Added event")
        return event

    def find_event(self, event_id):
        event = self.session.query(activities.Event).filter_by(id=event_id).first()
        return event

    def validate_event_exists(self, event_id):
        event = self.find_event(event_id)
        if event is None:
            raise ValueError("Event not found")
        return event

    @logger.log
    def edit_event(self, user, event_id, name=None, date=None, begin=None, end=None, place=None, is_public=None):
        event = self.validate_event_exists(event_id)
        user = self.session.merge(user)

        rights_validator.validate_event_edit_rights(user, event)

        if name is not None:
            event.name = name
        if date is not None and date != event.date:
            activity_controller.change_event_date(event, user, date)
            flag_modified(event, "date")
        if begin is not None:
            event.begin_time = begin
            flag_modified(event, "begin_time")
        if end is not None:
            event.end_time = end
            flag_modified(event, "end_time")
        if place is not None:
            event.place = place
        if is_public is not None:
            event.is_public = is_public

        try:
            validators.validate_date_begin_end_time(event.date, event.begin_time, event.end_time)
        except ValueError as e:
            self.session.rollback()
            raise e from None

        self.session.commit()
        logger.get_logger().info("Edited event")

    @logger.log
    def invite_user_to_event(self, inviter, event_id, user_name):
        user = self.validate_user_exists(user_name)
        inviter = self.session.merge(inviter)

        event = self.validate_event_exists(event_id)

        try:
            activity_controller.invite_user_to_event(event, inviter, user)
        except Exception as e:
            logger.get_logger().error(e)
            raise e from None
        self.session.commit()
        logger.get_logger().info(
            "User with ID {} was invited to event with ID {}".format(user.id, event_id))

    @logger.log
    def remove_event_member(self, remover, event_id, user_name):
        event = self.validate_event_exists(event_id)
        remover = self.session.merge(remover)

        if user_name is None or user_name == remover.name:
            user = remover
        else:
            user = self.validate_user_exists(user_name)
        rights_validator.validate_remove_event_member_rights(remover, user, event)

        event.remove_user(user)
        calendar_controller.remove_from_date(user.calendar, event, event.date)
        try:
            user.plans.remove(event.plan)
        except Exception:
            pass
        logger.get_logger().info("User {} removed from event with ID {}".format(user, event.id))
        self.session.commit()

    @logger.log
    def remove_event(self, user, event_id):
        event = self.validate_event_exists(event_id)

        rights_validator.validate_event_remove_rights(user, event)

        if event.creator is user:
            try:
                self.session.delete(event.plan)
            except Exception:
                pass
            self.session.delete(event)
        else:
            event.remove_user(user)
            try:
                user.plans.remove(event.plan)
            except Exception:
                pass
            calendar_controller.remove_from_date(user.calendar, event, event.date)

        self.session.commit()
        logger.get_logger().info("Removed event")

    @logger.log
    def create_task(self, user,
                    name=None,
                    deadline=None,
                    comment=None,
                    task_type=None,
                    status=activities.TaskStatus.NEW,
                    priority=activities.TaskPriority.MEDIUM,
                    list_name=None,
                    parent_id=None):
        if list_name is None:
            list_name = self.get_task_lists(user)[0].name
        if name is None:
            name = "Task"
        if status is None:
            status = activities.TaskStatus.NEW
        if priority is None:
            priority = activities.TaskPriority.MEDIUM

        if parent_id is not None:
            parent_task = self.validate_task_exists(parent_id)
            rights_validator.validate_create_subtask_rights(user, parent_task)
        else:
            parent_task = None

        task = user_controller.create_task(user, name, deadline, comment,
                                           task_type, status, priority, list_name, parent_task)

        self.session.commit()
        logger.get_logger().info("Added task")
        return task

    def find_task(self, task_id):
        task = self.session.query(activities.Task).filter_by(id=task_id).first()
        return task

    def find_task_by_params(self, user,
                            task_id=None,
                            name=None,
                            deadline=None,
                            begin=None,
                            end=None,
                            task_type=None,
                            status=None,
                            priority=None,
                            parent_id=None):
        user_tasks = self.session.query(activities.Task).all()
        user_tasks = [task for task in user_tasks
                      if task.creator is user or user in task.executors]
        tasks = []

        if task_id is not None:
            tasks.extend(task for task in user_tasks if task.id == task_id)
        if name is not None:
            tasks.extend(task for task in user_tasks if name.lower() in task.name.lower())
        if deadline is not None:
            tasks.extend(task for task in user_tasks if task.deadline == deadline)
        if begin is not None and end is not None:
            tasks.extend(task for task in user_tasks
                         if task.deadline is not None and begin <= task.deadline <= end)
        if begin is not None and end is None:
            tasks.extend(task for task in user_tasks
                         if task.deadline is not None and begin <= task.deadline)
        if begin is None and end is not None:
            tasks.extend(task for task in user_tasks
                         if task.deadline is not None and task.deadline <= end)
        if task_type is not None:
            tasks.extend(task for task in user_tasks if task.type == task_type)
        if status is not None:
            tasks.extend(task for task in user_tasks if task.status == status)
        if priority is not None:
            tasks.extend(task for task in user_tasks if task.priority == priority)
        if parent_id is not None:
            tasks.extend(task for task in user_tasks if task.parent_task_id == parent_id)

        return set(tasks)

    def validate_task_exists(self, task_id):
        task = self.find_task(task_id)
        if task is None:
            raise ValueError("Task not found")
        return task

    @logger.log
    def edit_task_status(self, user, task_id, status):
        task = self.validate_task_exists(task_id)
        user = self.session.merge(user)

        if status is not None:
            activity_controller.change_task_status(task, user, status)

            self.session.commit()
            logger.get_logger().info("Task status edited")

    @logger.log
    def add_task_executor(self, inviter, task_id, user_name):
        user = self.validate_user_exists(user_name)
        inviter = self.session.merge(inviter)
        task = self.validate_task_exists(task_id)
        activity_controller.add_task_executor(task, inviter, user)

        self.session.commit()
        logger.get_logger().info(
            "User with ID {} was added to executors of task with ID {}".format(user.id, task.id))

    @logger.log
    def remove_task_executor(self, remover, task_id, user_name):
        task = self.validate_task_exists(task_id)
        remover = self.session.merge(remover)

        rights_validator.validate_task_edit_remove_rights(remover, task)

        if user_name is None or user_name == remover.name:
            user = remover
        else:
            user = self.validate_user_exists(user_name)

        task.remove_executor(user)
        if task.deadline is not None:
            calendar_controller.remove_from_date(user.calendar, task, task.deadline)

        try:
            user.plans.remove(task.plan)
        except UnmappedInstanceError:
            pass
        except ValueError:
            pass

        logger.get_logger().info("User {} was deleted from task {} executors".format(user, task.id))
        self.session.commit()

    @logger.log
    def edit_task(self, user, task_id, name=None, deadline=None, comment=None, task_type=None, priority=None):
        task = self.validate_task_exists(task_id)
        user = self.session.merge(user)

        rights_validator.validate_task_edit_remove_rights(user, task)

        if name is not None:
            task.name = name
        if deadline is not None:
            activity_controller.change_task_deadline(task, user, deadline)
            flag_modified(task, "deadline")
        if comment is not None:
            task.comment = comment
        if task_type is not None:
            task.type = task_type
        if priority is not None:
            task.priority = priority

        self.session.commit()
        logger.get_logger().info("Task edited")

    @logger.log
    def remove_task(self, user, task_id):
        task = self.validate_task_exists(task_id)

        rights_validator.validate_task_edit_remove_rights(user, task)

        try:
            self.session.delete(task.plan)
        except UnmappedInstanceError:
            pass
        self.session.delete(task)
        self.session.commit()
        logger.get_logger().info("Task deleted")

    @logger.log
    def create_action_plan(self, user, action_id, period=plan.Period.EVERY_DAY, interval=1):
        action = self.validate_action_exists(action_id)
        if action.deadline is None:
            raise AttributeError("First set action deadline")
        action_plan = plan_controller.create_plan(action, period, interval)
        flag_modified(action, "deadline")
        plan_controller.add_plan_to_user(user, action_plan)
        logger.get_logger().info("Created action plan")
        self.session.commit()
        return action_plan

    @logger.log
    def create_task_plan(self, user, task_id, period=plan.Period.EVERY_DAY, interval=1):
        task = self.validate_task_exists(task_id)
        if task.deadline is None:
            raise AttributeError("First set task deadline")

        rights_validator.validate_task_edit_remove_rights(user, task)

        task_plan = plan_controller.create_plan(task, period, interval)
        flag_modified(task, "deadline")
        for member in set(task.executors + [task.creator]):
            plan_controller.add_plan_to_user(member, task_plan)
        logger.get_logger().info("Created task plan")
        self.session.commit()
        return task_plan

    @logger.log
    def create_event_plan(self, user, event_id, period=plan.Period.EVERY_DAY, interval=1):
        event = self.validate_event_exists(event_id)

        rights_validator.validate_event_edit_rights(user, event)

        event_plan = plan_controller.create_plan(event, period, interval)
        flag_modified(event, "date")
        for member in event.members:
            plan_controller.add_plan_to_user(member, event_plan)
        logger.get_logger().info("Created task plan")
        self.session.commit()
        return event_plan

    def find_plan(self, plan_id):
        return self.session.query(plan.Plan).filter_by(id=plan_id).first()

    def validate_plan_exists(self, plan_id):
        activity_plan = self.find_plan(plan_id)
        if activity_plan is None:
            raise ValueError("Plan not found")
        return activity_plan

    @logger.log
    def set_is_enable_plan(self, user, plan_id, is_enabled=True):
        activity_plan = self.validate_plan_exists(plan_id)

        rights_validator.validate_plan_edit_remove_rights(user, activity_plan)

        activity_plan.is_enabled = is_enabled
        self.session.commit()

    @logger.log
    def disable_plan(self, user, plan_id):
        self.set_is_enable_plan(user, plan_id, False)
        logger.get_logger().info("Plan is disabled")

    @logger.log
    def enable_plan(self, user, plan_id):
        self.set_is_enable_plan(user, plan_id, True)
        logger.get_logger().info("Plan is enabled")

    @logger.log
    def edit_plan_interval(self, user, plan_id, period, interval):
        activity_plan = self.validate_plan_exists(plan_id)

        rights_validator.validate_plan_edit_remove_rights(user, activity_plan)

        activity_plan.period = period
        activity_plan.interval = interval
        self.session.commit()
        logger.get_logger().info("Plan interval is edited")

    @logger.log
    def remove_plan(self, user, plan_id):
        activity_plan = self.validate_plan_exists(plan_id)

        rights_validator.validate_plan_edit_remove_rights(user, activity_plan)

        self.session.delete(activity_plan)
        self.session.commit()
        logger.get_logger().info("Plan is removed")

"""This module provides functions to validate if user has right to do something"""


from trackthat.library.model import (
    client,
    plan
)
from trackthat.library.controllers.activity_controller import AccessError


def validate_list_edit_remove_rights(user, user_list):
    if isinstance(user_list, client.TaskList) and user.id != user_list.creator.id:
        raise AccessError("User has no right to remove list")


def validate_event_edit_rights(user, event):
    if event.creator.id != user.id:
        raise AccessError("Only creator can edit event")


def validate_event_remove_rights(user, event):
    if user.id not in [member.id for member in event.members]:
        raise AccessError("Only creator can edit event")


def validate_remove_event_member_rights(user, member, event):
    if user.id != event.creator.id and member is not None and member.id != user.id:
        raise AccessError("User has no right to remove user from event")


def validate_task_edit_remove_rights(user, task):
    if user.id != task.creator.id:
        raise AccessError("User hasn't right to remove task executor or edit task")


def validate_create_subtask_rights(user, parent_task):
    if user.id != parent_task.creator.id and user.id not in [executor.id for executor in parent_task.executors]:
        raise AccessError("User hasn't right to create subtask")


def validate_plan_edit_remove_rights(user, activity_plan):
    if (isinstance(activity_plan, plan.EventPlan) and user.id != activity_plan.event.creator.id
            or isinstance(activity_plan, plan.TaskPlan) and user.id != activity_plan.task.creator.id):
        raise AccessError("User hasn't right edit plan")

"""This module contains operations with user calendar"""
import datetime

import trackthat.library.model.calendar as calendar


def add_to_date(user_calendar, activity, date=datetime.date.today()):
    if isinstance(user_calendar, calendar.Calendar):
        date_instance = user_calendar.get_date(date)
        if date_instance is None:
            date_instance = calendar.Date(date)
            user_calendar.add_date(date_instance)
        date_instance.add_activity(activity)


def remove_from_date(user_calendar, activity, date=datetime.date.today()):
    if isinstance(user_calendar, calendar.Calendar):
        date_instance = user_calendar.get_date(date)
        if date_instance is None:
            raise ValueError("Date %s hasn't any activities" % date)
        date_instance.remove_activity(activity)


def get_date(user_calendar, date=datetime.date.today()):
    if isinstance(user_calendar, calendar.Calendar):
        date_instance = user_calendar.get_date(date)
        if date_instance is None:
            raise ValueError("Date %s hasn't any activities" % date)
        return date_instance

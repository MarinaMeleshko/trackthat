import trackthat.library.model.client as client
from trackthat.library.model import plan
import trackthat.library.model.activities as activities
import trackthat.library.model.calendar as calendar


def create_plan(activity, period=plan.Period.EVERY_DAY, interval=1):
    """
    Create plan for activity
    E.g. period is EVERY_DAY, interval is 2
    Plan will repeat every two days
    """
    if isinstance(activity, activities.Action):
        activity_plan = plan.ActionPlan(activity, period, interval)
    elif isinstance(activity, activities.Event):
        activity_plan = plan.EventPlan(activity, period, interval)
    elif isinstance(activity, activities.Task):
        activity_plan = plan.TaskPlan(activity, period, interval)
    elif isinstance(activity, calendar.Notification):
        activity_plan = plan.NotificationPlan(activity, period, interval)
    return activity_plan


def add_plan_to_user(user, activity_plan):
    if (isinstance(user, client.User) and isinstance(activity_plan, plan.Plan)
            and activity_plan is not None and activity_plan not in user.plans):
        user.plans.append(activity_plan)

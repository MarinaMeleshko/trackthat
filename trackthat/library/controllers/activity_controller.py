"""This module provides operations with activities"""

import trackthat.library.model.activities as activities
import trackthat.library.model.client as client
import trackthat.library.controllers.calendar_controller as calendar_controller
import trackthat.library.controllers.plan_controller as plan_controller
from trackthat.library.model import validators


class AccessError(Exception):
    def __init__(self, message):
        self.message = message


def invite_user_to_event(event, inviter, user):
    """
    Creator can always invite users, 
    other members can invite users to public event
    """

    if (isinstance(event, activities.Event) and
            isinstance(inviter, client.User) and isinstance(user, client.User)):
        if event.creator is inviter:
            plan_controller.add_plan_to_user(user, event.plan)
            calendar_controller.add_to_date(user.calendar, event, event.date)
            event.invite_user(user)
        else:
            if not event.is_public:
                raise AccessError("Event is private")
            event.invite_user(user)
            plan_controller.add_plan_to_user(user, event.plan)
            calendar_controller.add_to_date(user.calendar, event, event.date)


def add_task_executor(task, inviter, executor):
    """
    Only task creator can add executors
    User can add himself as executor
    """

    if (isinstance(task, activities.Task) and
            isinstance(inviter, client.User) and isinstance(executor, client.User)):
        if inviter is executor or inviter is task.creator:
            task.add_executor(executor)
            plan_controller.add_plan_to_user(executor, task.plan)
            if task.deadline is not None:
                calendar_controller.add_to_date(executor.calendar, task, task.deadline)
        else:
            raise AccessError("Only creator can invite users")


def change_task_status(task, user, status):
    """
    Creator can always change task status
    Executors can set task status in IN_PROCESS, EXECUTED and FAILED
    """

    if isinstance(task, activities.Task) and isinstance(user, client.User):
        if user is task.creator:
            task.status = status
            if status is activities.TaskStatus.CLOSED:
                task.close()
        elif (user in task.executors and (status == activities.TaskStatus.IN_PROCESS or
                                          status == activities.TaskStatus.EXECUTED or
                                          status == activities.TaskStatus.FAILED)):
            task.status = status
        else:
            raise AccessError("User can't change task status")


def change_action_deadline(action, user, deadline):
    if isinstance(action, activities.Action) and isinstance(user, client.User):
        validators.validate_date(deadline)
        if action.deadline is not None:
            calendar_controller.remove_from_date(user.calendar, action, action.deadline)
        action.deadline = deadline
        calendar_controller.add_to_date(user.calendar, action, deadline)


def change_event_date(event, user, date):
    """Only creator can edit event"""

    if isinstance(event, activities.Event) and isinstance(user, client.User):
        if user is not event.creator:
            raise AccessError("Only creator can edit event")
        if event.date != date:
            validators.validate_date_begin_end_time(date, event.begin_time, event.end_time)
            for member in event.members:
                calendar_controller.remove_from_date(member.calendar, event, event.date)
                calendar_controller.add_to_date(member.calendar, event, date)
            event.date = date


def change_task_deadline(task, user, deadline):
    """Only creator can edit task deadline"""

    if isinstance(task, activities.Task) and isinstance(user, client.User):
        if user is not task.creator:
            raise AccessError("Only creator can edit task deadline")
        if task.deadline != deadline:
            validators.validate_date(deadline)
            for user in task.executors + [task.creator]:
                if task.deadline is not None:
                    calendar_controller.remove_from_date(user.calendar, task, task.deadline)
                calendar_controller.add_to_date(user.calendar, task, deadline)
            task.deadline = deadline

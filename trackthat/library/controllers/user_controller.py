"""
This module provides operations for activities creating
and list and folders managing
"""
import datetime

import trackthat.library.model.client as client
import trackthat.library.model.activities as activities
import trackthat.library.controllers.calendar_controller as calendar_controller
from trackthat.library.model import validators


def create_action(user, name="Action", deadline=None, comment=None, list_name="Action list"):
    if isinstance(user, client.User):
        validators.validate_date(deadline)
        action = activities.Action(name, deadline, comment)

        if deadline is not None:
            calendar_controller.add_to_date(user.calendar, action, deadline)

        action_list = next((l for l in user.activities if l.name == list_name), None)
        if action_list is None:
            action_list = create_action_list(user, list_name)
        action_list.add(action)
        return action


def create_event(user,
                 name="Event",
                 date=datetime.date.today(),
                 begin_time=None,
                 end_time=None,
                 place=None,
                 is_public=True):
    if isinstance(user, client.User):
        validators.validate_date_begin_end_time(date, begin_time, end_time)
        event = activities.Event(user, name, date, begin_time, end_time, place, is_public)
        calendar_controller.add_to_date(user.calendar, event, date)
        return event


def create_task(user,
                name="Task",
                deadline=None,
                comment=None,
                task_type=None,
                status=activities.TaskStatus.NEW,
                priority=activities.TaskPriority.MEDIUM,
                list_name="Task list",
                parent_task=None):
    if isinstance(user, client.User):
        validators.validate_date(deadline)
        task = activities.Task(user, name, deadline, comment, task_type, status, priority)

        if deadline is not None:
            calendar_controller.add_to_date(user.calendar, task, deadline)

        task_list = next((l for l in user.activities if l.name == list_name), None)
        if task_list is None:
            task_list = client.TaskList(list_name)
            user.activities.append(task_list)
        task_list.add(task)

        if parent_task is not None:
            parent_task.add_subtask(task)

        return task


def create_action_list(user, name="Action list"):
    if isinstance(user, client.User):
        if any(l.name == name for l in user.activities):
            raise ValueError("User has list with name %s" % name)
        return user.create_action_list(name)


def create_task_list(user, name="Task list"):
    if isinstance(user, client.User):
        if any(l.name == name for l in user.activities):
            raise ValueError("User has list with name %s" % name)
        return user.create_task_list(name)


def create_folder(user, name="Folder"):
    if isinstance(user, client.User):
        if any(l.name == name for l in user.activities):
            raise ValueError("User has folder with name %s" % name)
        return user.create_folder(name)

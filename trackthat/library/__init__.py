"""
This library provides entities and functions for action,
event and task tracker.
It logs info in logger with name 'trackthat logger',
and other logging settings can be overridden.

Library contains entities:

User:

    >>> import trackthat.library.model.client as client
    >>> user = client.User(["Username"])

Action:

    >>> import trackthat.library.model.activities as activities
    >>> action = activities.Action(["action_name"], [deadline], ["comment"])

Event:

    >>> event = activities.Event(user, ["event_name"], [date], [begin], [end], ["place"], [is_public])

Task:

    >>> task = activities.Task(user, ["name"], [deadline], ["comment"], [task_type], [status], [priority])

    For status use one of constants activities.TaskStatus
    Priority - activities.TaskPriority

Notification:

    >>> import trackthat.library.model.calendar as calendar
    >>> notification = calendar.Notification(["name"], [date], [time])

Plan:

    Is responsible for activities repeating
    First argument is activity
    Second argument is interval in seconds, you can use plan.Periodicity constants

    >>> import trackthat.library.model.plan as plan
    >>> action_plan = plan.ActionPlan(action, interval)

Action list:

    >>> action_list = client.ActionList(["name"])

Task list:

    >>> task_list = client.TaskList(user, ["name"])

Date:

    Date is an entity that stores actions, events, tasks and notifications.
    E.g. if action has deadline 12.06.2018 you can add it to date instance
    with date = 12.06.2018

    >>> import datetime
    >>> date_instance = calendar.Date(datetime.date(year=2018, month=6, day=12))
    >>> action = activities.Action("name", datetime.date(year=2018, month=6, day=12))
    >>> date_instance.add_activity(action)

Calendar:

    Calendar stores date collection

    >>> user_calendar = calendar.Calendar()

    Get date instance with date = 12.06.2018

    >>> user_calendar.get_date(datetime.date(year=2018, month=6, day=12))

    This method returns None if calendar hasn't date

TrackThatApi:

    TrackThatApi class is responsible for all operations with entities,
    saving and committing changes
    Any app interface can call TrackThatApi methods

    >>> import trackthat.library.trackthat_api as trackthat_api
    >>> api = trackthat_api.TrackThatApi(user, data_base_path)

    Create action on behalf of user with specified params:

    >>> api.create_action(user, "name", deadline, "comment", "list_name")

    Add and save user:

    >>> api.add_user(user)

    Create repeating plan on behalf of user for task with task_id:

    >>> api.create_task_plan(user, task_id, "day", 1)

"""


from trackthat.library.model import *
from trackthat.library.controllers import *
from trackthat.library import *


__version__ = '1.0.0'

__author__ = "Marina Meleshko"

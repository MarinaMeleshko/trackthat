from setuptools import setup
import os
import runpy


def get_version():
    filename = os.path.join(os.path.dirname(__file__), "trackthat", "__init__.py")
    var = runpy.run_path(filename)
    return var["__version__"]


_VERSION = get_version()


setup(
    name="trackthat",
    version=_VERSION,
    install_requires=["sqlalchemy"],
    description="Console application for task, event tracking and to do lists",
    author="Marina Meleshko",
    author_email="irmelen909@gmail.com",
    url="https://bitbucket.org/MarinaMeleshko/trackthat/",
    packages=["trackthat"],
    entry_points={
        "console_scripts": [
            "trackthat = trackthat.console.__main__:main"
        ]
    },
    test_suite="tests"
)

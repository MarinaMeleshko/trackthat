#TrackThat. Console action, event and task tracker#

TrackThat is console application to manage actions, events and tasks.

##Install##

First install setuptools:

```bash
$ pip install -U pip setuptools
```
Clone repository:

```bash
$ git clone https://MarinaMeleshko@bitbucket.org/MarinaMeleshko/trackthat.git

```
Installing app:

```bash
$ cd trackthat
$ python3 setup.py install
```

Run tests:

```bash
$ python3 setup.py test
```

##Work with users##

You can register new users, login, change current user

```bash
$ trackthat user [command]
```
For example, add new user:

```bash
$ trackthat user add username
```

Get info about yourself:

```bash
$ trackthat whoami
```

##Work with lists##

You can create, remove, rename lists

```bash
$ trackthat list [command]
```

Add new action list:

```bash
$ trackthat list add name="New list" -a
```

Show all task lists:

```bash
$ trackthat list all -t
```
Remove list:

```bash
$ trackthat list remove ListName
```
##Show date of calendar##

This command show activities for dates

```bash
$ trackthat date [params]
```
Show activities for today:

```bash
$ trackthat date
```

Show activities for 12.06.2018:

```bash
$ trackthat date --date=12.06.2018
$ trackthat date -d=12 -m=6 -y=2018
```
##Work with actions##

You can create, edit, remove actions

```bash
$ trackthat action [command][params]
```

##Work with events##

```bash
$ trackthat event [command][params]
```

Invite user to event:
```bash
$ trackthat event invite [event_id][user_name]
```

Remove event member or leave event:
```bash
$ trackthat event leave [event_id][user_name or nothing]
```
If instead of user_name you don't write anything you leave this event
##Work with tasks##

```bash
$ trackthat task [command][params]
```

create subtask:

```bash
$ trackthat task sub [parent_id][params]
```

Add task executor:
```bash
$ trackthat task addexec [task_id][user_name]
```

Remove task executor:
```bash
$ trackthat task removeexec [task_id][user_name]
```

##Work with repeating plans##

Activities can repeat. For activity repeating you must create plan:

```bash
$ trackthat plan add [id][period][type]
```
Type parameter is activity type: action (-a), event (-e), task (-t)

To show all your plans:
```bash
$ trackthat plan all
```
Edit plan:
```bash
$ trackthat plan edit [id][params]
$ trackthat plan disable [id]
```
Remove plan:
```bash
$ trackthat plan remove [id]
```
For more information write "-h" or "--help"
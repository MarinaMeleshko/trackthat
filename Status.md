﻿#Status#

##Аналоги##

###Списки дел###

Все приложения такого рода объединяют несколько базовых возможностей. К таковым относятся:

+ возможность создавать задачи
+ настраивать для них сроки выполнения
+ напоминания
+ создавать многократно повторяющиеся задачи
+ создавать именованные списки задач
+ создавать многопользовательские списки
+ прикреплять комментарии к задачам
+ объединять списки в папки

####Wunderlist####

Из того, что я изучала и пробовала, понравился мне больше всего. Включает все вышеперечисленные возможности, имеет приятный интерфейс, удобнее всего осуществляется объединение списков в папки и добавление новых пользователей в список. Минусы: на мой взгляд, не помешали бы вкладки сегодня, завтра, неделя.

####Todoist####

Тоже симпатичное приложение. К плюсам отнесу наличие вкладок сегодня, ближайшая неделя, и возможность выбрать цветовую тему. Минусы: не очень удобный способ добавления папок и списков в них, иногда не помешало бы свернуть меню.

####Any.do####

Один большой минус - нет навигации по папкам и спискам в меню слева, и пока работаешь с каким-то списком, нет возможности просто глянуть на иерархию задач, приходится переходить на главную страницу. Также можно только создавать подзадачи, но нельзя объединить списки в папку. Плюс - разделение задач по времени на сегодня, завтра, в ближайшее время и когда-нибудь.

####TickTick####

Также присутствуют все базовые возможности. Скорее плюс, чем минус - наличие отдельного большого поля для описания задачи. Иногда минус - поле описания нельзя свернуть, и иногда оно отвлекает и занимает много места.

###Календари###

По функциональности Google и Yandex календари ничем кардинально не отличаются.

+  Можно создавать события и описания у ним
+  Для событий можно назначать время, место, 
+  Создавать видеоконференцию к событию
+  События могут быть многопользовательскими
+  При добавлении участников к событию можно указать их полномочия
+  Настроить приватность и уведомления для события
+  Событие может быть повторяющимся планом
+  Можно импортировать другие календари, например, расписание БГУИР (эта возможность нравится мне больше всего)

Подобные сервисы подходят именно для отслеживания мероприятий с конкретными сроками.

###Трекеры задач###

Предназначены для назначения некоторых задач или поручений конкретным людям. 

Имеют следующие возможности:

+  создать задачу
+  назначить её исполнителей. Яндекс.Трекер предлагает ещё назначать наблюдателей
+  определить вид задачи
+  определить срочность и дедлайн
+  назначить и изменить статус в зависимости от вида задачи
+  оставлять комментарии к задаче
+  прикреплять метки для быстрого поиска
+  поиск по меткам, статусу, виду
+  создавать связи между задачами

Лично мне более удобным показалось использовать трекеры, предлагаемые сервисами BitBucket и GitHub. Баг-трекеры и трекеры задач используются в связи с какими-то проектами, в частности в IT, и доступ к проекту на том же сервисе - очевидный плюс. Для повседневного использования вполне могут быть заменены многопользовательскими списками в приложениях типа to-do-list.

##Сценарии использования##

+  Пользователь создаёт список дел. Добавляет туда дело, устанавливает для него дедлайн и напоминание за три дня до дедлайна. За три дня до дедлайна в календаре появляется запись об этом деле.
+  Пользователь назначает делу повторение и дни и время. Это может быть каждый день, определённые дни недели или дни месяца. В календаре в назначенные дни появляется запись об этом деле и времени. Например, ходить в тренажёрный зал по средам и пятницам в 18.00, принимать лекарство ежедневно в 9.00 и 21.00.
+  Пользователь хочет уже существующее дело добавить в календарь на нужную дату. Он на этой дате выбирает действие "Добавить существующее" и выбирает из существующих дел. У выбранного дела меняется дедлайн, а в календаре появляется запись о деле. При желании пользователь может также установить напоминания.
Например, у пользователя есть дело "Сделать покупку". Когда он накануне знает, что будет недалеко от нужного магазина, добавляет это дело в календарь на завтра с напоминанием в 9.00.
+  Пользователь может создать мероприятие в календаре. Например, поход в кинотеатр на сеанс 21.00-23.30. Тогда он указывает кинотеатр как место, время сеанса как время мероприятия.
+  Пользователь может создать повторяющееся мероприятие, например, собрание какого-нибудь клуба в 1-ю и 3-ю среду месяца. Место - клуб, время - 19.00-21.00, повторять по нечётным неделям месяца по средам.
+  В мероприятии могут участвовать несколько пользователей. Создатель мероприятия "Собрание клуба" может добавить в него членов клуба. Можно настроить приватность: если клуб закрытый, то нельзя приглашать на собрания других участников и, соответственно, добавлять их к мероприятию. Если по каким-то причинам собрание отменяется или переносится, председатель клуба может отредактировать время или место проведения собрания.
+  Пользователь хочет запланировать какое-то сложное дело, например, сделать ремонт. Он создаёт список "Ремонт", добавляет туда задачи-этапы ремонта с дедлайном.
+  К этапам ремонта пользователь может создавать подзадачи. Например, задача "Подготовительные работы". Подзадачи: найти помощников или строительную бригаду, вынести мебель и технику, снять старые покрытия, закупить инструменты и материалы. Когда выполнены все подзадачи, задача "Подготовительные работы" автоматически считается выполненной.
+  Пользователь хочет устроить сюрприз своему другу ко Дню рождения. Для этого он создаёт список задач, называет его "Днюха Эдика", добавляет в список всех своих и Эдика общих друзей. Создаёт задачи, назначает за них ответственных: Вася, Петя, Миша идут за подарком, Коля ответственный за еду и алкоголь. Задача "Придумать культурную программу" остаётся без исполнителя, и любой пользователь списка может сам назначиться на неё.
+  Создатель и исполнители задачи могут создавать подзадачи. Например, Коля, который отвечает за еду и алкоголь, может создать задачу "Купить любимый торт Эдика", и назначить исполнителем Витю.
+  Внезапно оказывается, что любимого торта Эдика нигде нет. Витя создаёт новую задачу "Купить другой торт!", добавляет к ней описание "Нигде нет нужного торта!", присваивает ей самый высокий приоритет, а задаче "Купить любимый торт Эдика" её создатель Коля присваивает статус "провалено".

##Планируемые возможности##

Готовое приложение будет сочетать возможности вышеперечисленных типов программ. Предполагается, что основной функционал будет как у календарей (то есть основной интерфейс, который видит пользователь - календарь). 

+  Создание списков дел 
+  Интеграция всех дел с пользовательским календарём. Если мы в список добавляем какое-то дело со сроком выполнения и настраиваем для него напоминание за три дня, например, то за три дня до дедлайна в календаре появляется запись об этом деле. И в день дедлайна появляется запись о том, что сегодня крайний срок. 
+  Можно создать повторяющееся дело, настроить дни и время повторения и напоминания. Тогда в нужные дни и время напоминания об этом деле появятся в календаре (под напоминанием подразумевается запись в календаре на нужную дату и время).
+ Дела, для которых не установлен дедлайн, автоматически в календарь не добавляются, но это можно сделать вручную. 
+ Возможность создавать мероприятия в календаре, назначать для них место, время.
+ Мероприятия также могут быть повторяющимися.
+ В мероприятии может участвовать несколько человек, которых может приглашать создатель мероприятия. Создатель мероприятия может разрешить или запретить участникам приглашать новых участников, назначать место, отменять или переносить мероприятие.  
+ Возможность создавать списки задач, которые могут быть многопользовательскими. 
+ Интеграция задач с календарём
+ Если создатель списка назначает ответственных за задачу пользователей, им приходит уведомление, они могут назначить срок напоминания, и в их календарях появляется соответствующая запись. Также пользователь сможет сам войти в список ответственных за задачу, если её создатель уже таковых не назначил. 
+ У задачи будет тип и статус. Тип задаётся пользователем, создавшим задачу, а статус может меняться в процессе выполнения. 
+ Создатель задачи может изменить её статус как угодно, ответственные за задачу могут изменить её статус только на "исполнено". Можно будет задать приоритет задачи. 
+ Ответственные за задачу и её создатель могут создавать для неё подзадачи. При закрытии всех подзадач задача автоматически закрывается.
+ Каждый пользователь может создать свою иерархию папок. Папка может содержать списки дел, списки задач и другие папки. Списки дел и задач могут существовать и вне папок.

##Логическая архитектура##

Архитектуру программы будут составлять следующие сущности:

+ *Пользователь.* Собственно, владелец своего календаря, всех своих списков дел и задач, их папок. 

    Хранит:

    + Имя и другие персональные и данные для входа
    + Календарь
    + Списки дел
    + Списки задач, к которым имеет доступ
    + Папки
    
+ *Дело.* Сущность, представляющая запланированное действие. Может быть установлен дедлайн, или же быть без срока выполнения. Обязательно входит в состав одного или нескольких списков дел, о которых речь пойдёт ниже. Может иметь ни одного, одно или несколько напоминаний.

    Дело может быть повторяющимся планом. Тогда по заданным параметрам повторения в нужные дни и часы автоматически создаются напоминания.

    У дела может быть также комментарий, который показывается в напоминаниях.

    Хранит:

    + Название
    + Комментарий
    + Дедлайн, если он установлен
    + Время повторений, если дело повторяющееся
    + Напоминания (которые генерируются автоматически у повторяющегося дела, либо при создании, если дело обычное)

+ *Напоминание.* Отвечает за запись в календаре. Обязательно должны быть установлены дата и время. При создании напоминания в установленную дату и время в календарь записываются данные о деле, для которого оно установлено.
    
    Хранит:

    + Название
    + Комментарий
    + Дата и время

+ *Список дел.* Список дел имеет название. Дела не могут существовать вне списка.
    
    Хранит:

    + Название
    + Дела
    
+ *Задача.* Сущность, представляющая поручение, у которого есть создатель и ни одного, один или несколько ответственных. Имеет тип и приоритет, может иметь комментарий. Задачи существуют внутри списков задач (см. ниже). Задача имеет дедлайн и также может иметь напоминания. 
    
    Как только задаче создателем назначается ответственный, последнему приходит об этом уведомление. Он тогда может настраивать время напоминаний у себя в календаре. 

    Создатель задачи может изменять её статус как пожелает. Ответственные за задачу могут изменить её статус только на "исполнено".

    Создатель и ответственные за неё могу создавать подзадачи. Если создаётся подзадача, то это будет объект того же класса задачи, но ссылка на него сохраняется в родительской задаче. Когда всем подзадачам присвоен статус "закрыто", родительской задаче автоматически присваивается статус "закрыто". Для осуществления этого задача хранит ссылку на родительскую задачу, если таковая имеется. При закрытии подзадачи счётчик открытых подзадач в родительской задачи уменьшается. Родительская задача автоматически закрывается при обнулении счётчика своих подзадач.

    Хранит:

    + Название
    + Комментарий
    + Создателя
    + Список исполнителей
    + Тип
    + Приоритет
    + Статус
    + Дедлайн
    + Напоминания
    + Список подзадач
    + Счётчик незакрытых подзадач

+ *Список задач.* Список задач имеет название. Задачи не могут существовать вне списка.
    
    Списки задач могут быть многопользовательскими. При создании списка задач у него есть по умолчанию один пользователь - создатель. Он может добавлять в список других пользователей.

    Создатель списка может создавать задачи и назначать ответственных за них из числа пользователей списка. Если у задачи нет ответственных, любой пользователь списка может сам назначиться ответственным за задачу.

    Хранит:

    + Название
    + Задачи
    + Создателя
    + Пользователей

+ *Папка.* Пользователь может создавать свою иерархию папок. Папка имеет название (назначенное пользователем или дефолтное) и содержать списки дел, списки задач и другие папки (точнее, ссылки на них).
    
    Хранит:

    + Название
    + Списки дел
    + Списки задач
    + Папки
    
+ *Мероприятие.* Сущность, отвечающая за событие, назначенное на конкретное время. Обязательно имеет дату и время начало, может быть также установлено время окончания. Может быть назначено место. Обязательно имеет создателя. Создатель может добавить участников и определить, могут ли другие участники добавлять участников.

    Когда пользователя добавляют в список участников мероприятия, в его календаре появляется запись об этом на нужную дату и время. Пользователь может в своём календаре поставить напоминания на нужные даты об этом мероприятии.

    Создатель мероприятия может его отменить и/или перенести на другую дату.

    Хранит:

    + Название
    + Дата
    + Время начала
    + Время окончания
    + Место
    + Создатель
    + Тип приватности
    + Участники
    + Напоминания
    + Время повторений, если мероприятие повторяется

+ *Дата.* Существует только в коллекции дат календаря пользователя. Хранит день, месяц, год, за которые отвечает. Имеет ссылки на дела, задачи, мероприятия и напоминания, назначенные на неё.

    Хранит:

    + Дата, за которую отвечает
    + Дела
    + Задачи
    + Мероприятия
    + Напоминания

+ *Календарь.* Каждый пользователь обязательно его имеет. Календарь содержит коллекцию дат. 

    Если на какую-то дату назначается мероприятие, дедлайн по делу, дедлайн по задаче или напоминание о чем-либо, то дата, если её нет в коллекции дат календаря, создаётся. В неё записывается ссылка на то, что назначается на эту дату, и дата добавляется в коллекцию. 

    Если дата, на которую назначается некоторая сущность, уже есть в коллекции дат, то нужной существующей дате добавляется ссылка на эту сущность.

    Хранит:

    + Коллекцию дат, на которые что-то назначено

##Этапы разработки и сроки##

|Этап разработки  |        Подробности        |Крайний срок начала| Дедлайн| Фактическая дата сдачи|
|-----------------|---------------------------|------|-------|----------------|
|Создание модели|Создание всех необходимых для осуществления базовой логики классов, которые описаны выше в разделе "Логическая архитектура". Описание всех методов взаимодействия. Написаны все классы, составляющие логику приложения, со всеми методами их взаимодействия.|15.03.2018|28.03.2018||
|Создание прототипа|Создание первой рабочей версии приложения, работающего через командную строку и хранящего данные пока в файлах.<br><br>Программа минимум: создание списков однократных дел и списков задач для одного пользователя, интеграция с календарём пользователя. Возможность создания иерархии однопользовательских задач. Создание однократных мероприятий в календаре без возможности приглашения других пользователей. Создание пользовательского календаря и возможность ответа на запрос о планах на любую дату. <br><br>Программа максимум: повторяющиеся дела и мероприятия, возможность создавать иерархию папок.|28.03.2018|13.04.2018||
|Создание расширенной версии|Уточнение и развитие внутренней логики приложения, добавление работы с базой данных.<br><br>Программа минимум: улучшение модели событий и напоминаний, совершенствование интеграции с календарём пользователя. Регистрация пользователей и многопользовательские списки задач и мероприятия, реализованные с добавлением базы данных. Повторяющиеся дела и мероприятия, настройка напоминаний к ним. <br><br>Программа максимум: синхронизация напоминаний в многопользовательских списках задач и мероприятиях и их интеграция с календарями других пользователей. |15.04.2018|05.05.2018||
|Создание веб-версии|Создание веб-интерфейса, совершенствование логики и улучшение синхронизации приложения.<br><br>Программа минимум: создание веб-интерфейса для работы со всем уже имеющимся функционалом. Синхронизации напоминаний в многопользовательских списках задач и мероприятиях, настройка каждым пользователем напоминаний в многопользовательских сущностях. <br><br>Программа максимум: комментарии к задачам в многопользовательских списках, обсуждения к многопользовательским мероприятиям. Элементарный мессенджер внутри приложения. Интеграция приложения с электронной почтой, возможность настройки уведомлений внутри приложения и по электронной почте.|07.05.2018|07.06.2018||

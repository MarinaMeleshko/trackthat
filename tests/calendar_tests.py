import unittest

import trackthat.library.model.calendar as calendar


class CalendarTestCase(unittest.TestCase):
    def setUp(self):
        self.calendar = calendar.Calendar()

    def test_get_date(self):
        self.assertEqual(self.calendar.get_date(), None)
        date_today = calendar.Date()
        self.calendar.add_date(date_today)
        self.assertIn(date_today, self.calendar.dates)


if __name__ == '__main__':
    unittest.main()

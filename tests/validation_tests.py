import unittest
import datetime

import trackthat.library.model.validators as validators


class ValidateTestCase(unittest.TestCase):
    def test_validate_date(self):
        yesterday = datetime.date.today() - datetime.timedelta(days=1)
        with self.assertRaises(ValueError):
            validators.validate_date(yesterday)

    def test_validate_begin_time_earlier_end_time(self):
        now = datetime.datetime.now()
        earlier = (now - datetime.timedelta(hours=1)).time()
        now = now.time()
        with self.assertRaises(ValueError):
            validators.validate_begin_time_earlier_end_time(now, earlier)

    def test_validate_date_begin_end_time(self):
        today = datetime.date.today()
        now = datetime.datetime.now()
        earlier = (now - datetime.timedelta(hours=1)).time()
        now = now.time()
        with self.assertRaises(ValueError):
            validators.validate_date_begin_end_time(today, earlier, None)
        with self.assertRaises(ValueError):
            validators.validate_date_begin_end_time(today, None, now)
        with self.assertRaises(ValueError):
            validators.validate_date_begin_end_time(today, earlier, now)


if __name__ == '__main__':
    unittest.main()

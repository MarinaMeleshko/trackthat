import unittest
import datetime

import trackthat.library.controllers.user_controller as controller
import trackthat.library.model.client as client


class UserControllerTestCase(unittest.TestCase):
    def setUp(self):
        self.user = client.User()

    def test_create_action(self):
        today = datetime.date.today()
        controller.create_action(self.user, deadline=today)
        action_list = next(l for l in self.user.activities if l.name == "Action list")
        action = next((a for a in action_list.content if a.name == "Action"), None)
        self.assertNotEqual(action, None)

    def test_create_event(self):
        controller.create_event(self.user)
        date_today = self.user.calendar.get_date()
        event = next((e for e in date_today.activities if e.name == "Event"), None)
        self.assertNotEqual(event, None)

    def test_create_task(self):
        today = datetime.date.today()
        controller.create_task(self.user, deadline=today)
        task_list = next(l for l in self.user.activities if l.name == "Task list")
        task = next((t for t in task_list.content if t.name == "Task"), None)
        self.assertNotEqual(task, None)

    def test_create_action_list(self):
        with self.assertRaises(ValueError):
            controller.create_action_list(self.user)


if __name__ == '__main__':
    unittest.main()

import unittest

import trackthat.library.model.activities as activities


class TaskTestCase(unittest.TestCase):
    def setUp(self):
        self.task = activities.Task(None)

    def test_add_subtask(self):
        subtask = activities.Task(None)
        self.task.add_subtask(subtask)
        self.assertIn(subtask, self.task.subtasks)
        self.assertEqual(self.task, subtask.parent_task)

    def test_close(self):
        subtask = activities.Task(None)
        self.task.add_subtask(subtask)
        subtask.close()
        self.assertEqual(subtask.status, "closed")
        self.assertEqual(self.task.status, "closed")


if __name__ == '__main__':
    unittest.main()

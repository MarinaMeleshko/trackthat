import unittest
import datetime

import trackthat.library.controllers.calendar_controller as controller
import trackthat.library.model.calendar as calendar
import trackthat.library.model.activities as activities


class CalendarControllerTestCase(unittest.TestCase):

    def setUp(self):
        self.calendar = calendar.Calendar()

    def test_add_to_date(self):
        today = datetime.date.today()
        action = activities.Action(deadline=today)
        controller.add_to_date(self.calendar, action)
        self.assertIn(action, self.calendar.get_date().activities)

        new_action = activities.Action(name="New action", deadline=today)
        controller.add_to_date(self.calendar, new_action)
        self.assertIn(new_action, self.calendar.get_date().activities)

    def test_remove_from_date(self):
        today = datetime.date.today()
        action = activities.Action(deadline=today)
        with self.assertRaises(ValueError):
            controller.remove_from_date(self.calendar, action)

        controller.add_to_date(self.calendar, action)
        controller.remove_from_date(self.calendar, action)
        self.assertNotIn(action, self.calendar.get_date().activities)

    def test_get_date(self):
        today = datetime.date.today()
        self.calendar.add_date(calendar.Date(today))

        tomorrow = today + datetime.timedelta(days=1)
        with self.assertRaises(ValueError):
            controller.get_date(self.calendar, tomorrow)


if __name__ == '__main__':
    unittest.main()

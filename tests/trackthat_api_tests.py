import unittest
import datetime

import trackthat.library.trackthat_api as api
import trackthat.library.logger as logger
import trackthat.library.model.client as client
import trackthat.library.model.activities as activities
import trackthat.library.controllers.activity_controller as activity_controller


class TrackThatApiTestCase(unittest.TestCase):
    def setUp(self):
        logger.set_logging_settings(logger_enabled=False)
        self.user = client.User()
        
        self.api = api.TrackThatApi(None, "sqlite:///:memory:")
        self.api.add_user(self.user)

    def test_create_action_list(self):
        action_list = self.api.create_action_list(self.user, "List")
        self.assertIn(action_list, self.api.session.query(client.ActionList))
        with self.assertRaises(ValueError):
            self.api.create_action_list(self.user, "List")

    def test_get_list(self):
        created_list = self.api.create_action_list(self.user, "List")
        action_list = self.api.get_list(self.user, "List")
        self.assertEqual(action_list, created_list)

    def test_remove_list(self):
        with self.assertRaises(ValueError):
            self.api.remove_list(self.user, "List")

        action_list = self.api.create_action_list(self.user, "List")
        action = self.api.create_action(self.user, name="Action", list_name="List")
        self.api.remove_list(self.user, "List")

        self.assertNotIn(action_list, self.api.session.query(client.ActionList))
        self.assertNotIn(action, self.api.session.query(activities.Action))

    def test_rename_list(self):
        with self.assertRaises(ValueError):
            self.api.rename_list(self.user, "Old", "New")

        self.api.create_action_list(self.user, "Old")
        self.api.create_action_list(self.user, "Old_1")
        with self.assertRaises(ValueError):
            self.api.rename_list(self.user, "Old", "Old_1")

        action_list = self.api.rename_list(self.user, "Old", "New")
        self.assertEqual(action_list.name, "New")

    def test_create_action(self):
        action = self.api.create_action(self.user, name="Action")
        self.assertIn(action, self.api.get_list(self.user, "Action list").content)

    def test_edit_action(self):
        today = datetime.date.today()
        tomorrow = today + datetime.timedelta(days=1)

        action = self.api.create_action(self.user, name="Action", deadline=today)
        self.api.edit_action(self.user, action.id, deadline=tomorrow)

        self.assertNotIn(action, self.api.get_date(self.user, today).activities)
        self.assertIn(action, self.api.get_date(self.user, tomorrow).activities)

    def test_remove_action(self):
        today = datetime.date.today()
        action = self.api.create_action(self.user, name="Action", deadline=today)
        self.api.remove_action(action.id)
        self.assertNotIn(action, self.api.get_date(self.user, today).activities)
        self.assertNotIn(action, self.api.get_list(self.user, "Action list").content)
        self.assertNotIn(action, self.api.session.query(activities.Action))

    def test_edit_event(self):
        today = datetime.date.today()
        tomorrow = today + datetime.timedelta(days=1)
        event = self.api.create_event(self.user, name="Event", date=today)

        self.api.edit_event(self.user, event.id, date=tomorrow)
        self.assertIn(event, self.api.get_date(self.user, tomorrow).activities)
        self.assertNotIn(event, self.api.get_date(self.user, today).activities)

        member = client.User("Member")
        event.members.append(member)
        with self.assertRaises(Exception):
            self.api.edit_event(member, event.id, date=today)

    def test_remove_event(self):
        today = datetime.date.today()
        event = self.api.create_event(self.user, name="Event", date=today, is_public=True)

        member = client.User("Member")
        activity_controller.invite_user_to_event(event, event.creator, member)
        self.api.remove_event(member, event.id)
        self.assertNotIn(event, self.api.get_date(member, today).activities)
        self.assertIn(event, self.api.session.query(activities.Event))
        self.assertNotIn(member, event.members)

        member_2 = client.User("Member2")
        activity_controller.invite_user_to_event(event, event.creator, member_2)

        self.api.remove_event(event.creator, event.id)
        self.assertNotIn(event, member_2.calendar.get_date(today).activities)
        self.assertNotIn(event, self.api.session.query(activities.Event))

    def test_create_task(self):
        task = self.api.create_task(self.user, name="Task")
        self.assertIn(task, self.api.get_list(self.user, "Task list").content)

    def test_create_subtask(self):
        task = self.api.create_task(self.user, name="Task")
        executor = client.User()
        task.add_executor(executor)
        other_user = client.User()

        sub_task = self.api.create_task(self.user, name="Subtask", parent_id=task.id)
        self.assertIn(sub_task, task.subtasks)

        new_sub_task = self.api.create_task(executor, name="Subtask", parent_id=task.id)
        self.assertIn(new_sub_task, task.subtasks)

        with self.assertRaises(Exception):
            self.api.create_task(other_user, name="Subtask", parent_id=task.id)


if __name__ == '__main__':
    unittest.main()

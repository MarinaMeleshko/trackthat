import unittest
import datetime

import trackthat.library.model.activities as activities
import trackthat.library.controllers.plan_controller as plan_controller


class PlanControllerTestCase(unittest.TestCase):
    def test_create_plan(self):
        today = datetime.date.today()
        action = activities.Action(deadline=today)
        activity_plan = plan_controller.create_plan(action)
        self.assertEqual(activity_plan, action.plan)
        self.assertEqual(action.deadline, today)


if __name__ == '__main__':
    unittest.main()

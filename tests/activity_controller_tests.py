import unittest
import datetime

import trackthat.library.controllers.activity_controller as controller
import trackthat.library.controllers.user_controller as user_controller
import trackthat.library.model.activities as activities
import trackthat.library.model.client as client


class ActivityControllerTestCase(unittest.TestCase):
    def test_invite_user_to_event(self):
        creator = client.User()
        event = activities.Event(creator=creator)
        event.is_public = False
        member = client.User("Member")
        controller.invite_user_to_event(event, creator, member)
        self.assertIn(member, event.members)

        new_user = client.User("New")
        with self.assertRaises(Exception):
            controller.invite_user_to_event(event, member, new_user)

    def test_add_task_executor(self):
        creator = client.User()
        task = activities.Task(creator=creator)
        executor = client.User("Executor")
        controller.add_task_executor(task, executor, executor)
        self.assertIn(executor, task.executors)

        new_executor = client.User("New")
        with self.assertRaises(Exception):
            controller.add_task_executor(task, executor, new_executor)

    def test_change_task_status(self):
        creator = client.User()
        executor = client.User("Executor")
        task = activities.Task(creator=creator)
        controller.add_task_executor(task, creator, executor)
        controller.change_task_status(task, creator, activities.TaskStatus.IN_PROCESS)
        self.assertEqual(task.status, activities.TaskStatus.IN_PROCESS)

        controller.change_task_status(task, executor, activities.TaskStatus.EXECUTED)
        self.assertEqual(task.status, activities.TaskStatus.EXECUTED)

        with self.assertRaises(Exception):
            controller.change_task_status(task, executor, activities.TaskStatus.CLOSED)

    def test_change_event_date(self):
        today = datetime.date.today()
        tomorrow = today + datetime.timedelta(days=1)
        creator = client.User()
        member = client.User()
        event = user_controller.create_event(creator, date=today)
        controller.invite_user_to_event(event, creator, member)

        with self.assertRaises(Exception):
            controller.change_event_date(event, member, tomorrow)

        controller.change_event_date(event, creator, tomorrow)
        self.assertNotIn(event, creator.calendar.get_date(today).activities)
        self.assertNotIn(event, member.calendar.get_date(today).activities)
        self.assertIn(event, creator.calendar.get_date(tomorrow).activities)
        self.assertIn(event, member.calendar.get_date(tomorrow).activities)


if __name__ == '__main__':
    unittest.main()

import unittest
import datetime
from dateutil.relativedelta import relativedelta

import trackthat.library.model.activities as activities
import trackthat.library.model.plan as plan
import trackthat.library.model.calendar as calendar


class PlanTestCase(unittest.TestCase):
    def test_create_action_plan(self):
        today = datetime.datetime.now()
        yesterday = (today - datetime.timedelta(days=1)).date()
        action = activities.Action(deadline=yesterday)
        plan.ActionPlan(action, plan.Period.EVERY_DAY)
        self.assertEqual(action.deadline, today.date())

        deadline = datetime.date.today() - relativedelta(months=1)
        action = activities.Action(deadline=deadline)
        plan.ActionPlan(action, plan.Period.EVERY_MONTH)
        self.assertEqual(action.deadline, today.date())

    def test_notification_update(self):
        now = datetime.datetime.now()
        earlier = now - datetime.timedelta(hours=12)
        notif = calendar.Notification(date=earlier.date(), time=earlier.time())
        plan.NotificationPlan(notif, plan.Period.EVERY_HOUR, 12)
        time = now + datetime.timedelta(hours=12)
        self.assertEqual(notif.date, time.date())
        self.assertEqual(notif.time, time.time().replace(microsecond=0))


if __name__ == '__main__':
    unittest.main()
